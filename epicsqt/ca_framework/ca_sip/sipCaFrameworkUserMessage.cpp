/*
 * Interface wrapper code.
 *
 * Generated by SIP 4.14.6 on Fri Sep 25 15:15:27 2015
 */

#include "sipAPICaFramework.h"

#line 7 "UserMessage.sip"
#include <UserMessage.h>
#line 12 "./sipCaFrameworkUserMessage.cpp"

#line 34 "C:\\Python26\\sip\\PyQt4/QtCore/qobject.sip"
#include <qobject.h>
#line 16 "./sipCaFrameworkUserMessage.cpp"
#line 36 "C:\\Python26\\sip\\PyQt4/QtCore/qstring.sip"
#include <qstring.h>
#line 19 "./sipCaFrameworkUserMessage.cpp"
#line 31 "C:\\Python26\\sip\\PyQt4/QtCore/qcoreevent.sip"
#include <qcoreevent.h>
#line 22 "./sipCaFrameworkUserMessage.cpp"
#line 329 "C:\\Python26\\sip\\PyQt4/QtCore/qcoreevent.sip"
#include <qcoreevent.h>
#line 25 "./sipCaFrameworkUserMessage.cpp"
#line 312 "C:\\Python26\\sip\\PyQt4/QtCore/qcoreevent.sip"
#include <qcoreevent.h>
#line 28 "./sipCaFrameworkUserMessage.cpp"
#line 265 "C:\\Python26\\sip\\PyQt4/QtCore/qvariant.sip"
#include <qvariant.h>
#line 31 "./sipCaFrameworkUserMessage.cpp"
#line 33 "C:\\Python26\\sip\\PyQt4/QtCore/qlist.sip"
#include <qlist.h>
#line 34 "./sipCaFrameworkUserMessage.cpp"
#line 37 "C:\\Python26\\sip\\PyQt4/QtCore/qbytearray.sip"
#include <qbytearray.h>
#line 37 "./sipCaFrameworkUserMessage.cpp"
#line 35 "C:\\Python26\\sip\\PyQt4/QtCore/qnamespace.sip"
#include <qnamespace.h>
#line 40 "./sipCaFrameworkUserMessage.cpp"
#line 120 "C:\\Python26\\sip\\PyQt4/QtCore/qlist.sip"
#include <qlist.h>
#line 43 "./sipCaFrameworkUserMessage.cpp"
#line 31 "C:\\Python26\\sip\\PyQt4/QtCore/qthread.sip"
#include <qthread.h>
#line 46 "./sipCaFrameworkUserMessage.cpp"
#line 35 "C:\\Python26\\sip\\PyQt4/QtCore/qregexp.sip"
#include <qregexp.h>
#line 49 "./sipCaFrameworkUserMessage.cpp"
#line 31 "C:\\Python26\\sip\\PyQt4/QtCore/qobjectdefs.sip"
#include <qobjectdefs.h>
#line 52 "./sipCaFrameworkUserMessage.cpp"


class sipUserMessage : public UserMessage
{
public:
    sipUserMessage(QObject *);
    virtual ~sipUserMessage();

    int qt_metacall(QMetaObject::Call,int,void **);
    void *qt_metacast(const char *);
    const QMetaObject *metaObject() const;

    /*
     * There is a public method for every protected method visible from
     * this class.
     */
    QObject * sipProtect_sender() const;
    int sipProtect_receivers(const char *) const;
    void sipProtectVirt_timerEvent(bool,QTimerEvent *);
    void sipProtectVirt_childEvent(bool,QChildEvent *);
    void sipProtectVirt_customEvent(bool,QEvent *);
    void sipProtectVirt_connectNotify(bool,const char *);
    void sipProtectVirt_disconnectNotify(bool,const char *);
    int sipProtect_senderSignalIndex() const;

    /*
     * There is a protected method for every virtual method visible from
     * this class.
     */
protected:
    bool event(QEvent *);
    bool eventFilter(QObject *,QEvent *);
    void timerEvent(QTimerEvent *);
    void childEvent(QChildEvent *);
    void customEvent(QEvent *);
    void connectNotify(const char *);
    void disconnectNotify(const char *);

public:
    sipSimpleWrapper *sipPySelf;

private:
    sipUserMessage(const sipUserMessage &);
    sipUserMessage &operator = (const sipUserMessage &);

    char sipPyMethods[7];
};

sipUserMessage::sipUserMessage(QObject *a0): UserMessage(a0), sipPySelf(0)
{
    memset(sipPyMethods, 0, sizeof (sipPyMethods));
}

sipUserMessage::~sipUserMessage()
{
    sipCommonDtor(sipPySelf);
}

const QMetaObject *sipUserMessage::metaObject() const
{
    return sip_CaFramework_qt_metaobject(sipPySelf,sipType_UserMessage);
}

int sipUserMessage::qt_metacall(QMetaObject::Call _c,int _id,void **_a)
{
    _id = UserMessage::qt_metacall(_c,_id,_a);

    if (_id >= 0)
        _id = sip_CaFramework_qt_metacall(sipPySelf,sipType_UserMessage,_c,_id,_a);

    return _id;
}

void *sipUserMessage::qt_metacast(const char *_clname)
{
    return (sip_CaFramework_qt_metacast && sip_CaFramework_qt_metacast(sipPySelf,sipType_UserMessage,_clname)) ? this : UserMessage::qt_metacast(_clname);
}

bool sipUserMessage::event(QEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[0],sipPySelf,NULL,sipName_event);

    if (!sipMeth)
        return QObject::event(a0);

    typedef bool (*sipVH_QtCore_5)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QEvent *);

    return ((sipVH_QtCore_5)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[5]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

bool sipUserMessage::eventFilter(QObject *a0,QEvent *a1)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[1],sipPySelf,NULL,sipName_eventFilter);

    if (!sipMeth)
        return QObject::eventFilter(a0,a1);

    typedef bool (*sipVH_QtCore_18)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QObject *,QEvent *);

    return ((sipVH_QtCore_18)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[18]))(sipGILState, 0, sipPySelf, sipMeth, a0, a1);
}

void sipUserMessage::timerEvent(QTimerEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[2],sipPySelf,NULL,sipName_timerEvent);

    if (!sipMeth)
    {
        QObject::timerEvent(a0);
        return;
    }

    typedef void (*sipVH_QtCore_9)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QTimerEvent *);

    ((sipVH_QtCore_9)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[9]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipUserMessage::childEvent(QChildEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[3],sipPySelf,NULL,sipName_childEvent);

    if (!sipMeth)
    {
        QObject::childEvent(a0);
        return;
    }

    typedef void (*sipVH_QtCore_25)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QChildEvent *);

    ((sipVH_QtCore_25)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[25]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipUserMessage::customEvent(QEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[4],sipPySelf,NULL,sipName_customEvent);

    if (!sipMeth)
    {
        QObject::customEvent(a0);
        return;
    }

    typedef void (*sipVH_QtCore_17)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QEvent *);

    ((sipVH_QtCore_17)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[17]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipUserMessage::connectNotify(const char *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[5],sipPySelf,NULL,sipName_connectNotify);

    if (!sipMeth)
    {
        QObject::connectNotify(a0);
        return;
    }

    typedef void (*sipVH_QtCore_24)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, const char *);

    ((sipVH_QtCore_24)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[24]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipUserMessage::disconnectNotify(const char *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[6],sipPySelf,NULL,sipName_disconnectNotify);

    if (!sipMeth)
    {
        QObject::disconnectNotify(a0);
        return;
    }

    typedef void (*sipVH_QtCore_24)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, const char *);

    ((sipVH_QtCore_24)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[24]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

QObject * sipUserMessage::sipProtect_sender() const
{
    return QObject::sender();
}

int sipUserMessage::sipProtect_receivers(const char *a0) const
{
    return QObject::receivers(a0);
}

void sipUserMessage::sipProtectVirt_timerEvent(bool sipSelfWasArg,QTimerEvent *a0)
{
    (sipSelfWasArg ? QObject::timerEvent(a0) : timerEvent(a0));
}

void sipUserMessage::sipProtectVirt_childEvent(bool sipSelfWasArg,QChildEvent *a0)
{
    (sipSelfWasArg ? QObject::childEvent(a0) : childEvent(a0));
}

void sipUserMessage::sipProtectVirt_customEvent(bool sipSelfWasArg,QEvent *a0)
{
    (sipSelfWasArg ? QObject::customEvent(a0) : customEvent(a0));
}

void sipUserMessage::sipProtectVirt_connectNotify(bool sipSelfWasArg,const char *a0)
{
    (sipSelfWasArg ? QObject::connectNotify(a0) : connectNotify(a0));
}

void sipUserMessage::sipProtectVirt_disconnectNotify(bool sipSelfWasArg,const char *a0)
{
    (sipSelfWasArg ? QObject::disconnectNotify(a0) : disconnectNotify(a0));
}

int sipUserMessage::sipProtect_senderSignalIndex() const
{
    return QObject::senderSignalIndex();
}


extern "C" {static PyObject *meth_UserMessage_sender(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_sender(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_UserMessage, &sipCpp))
        {
            QObject *sipRes = 0;

#line 646 "C:\\Python26\\sip\\PyQt4/QtCore/qobject.sip"
        typedef QObject *(*helper_func)(QObject *);
        
        static helper_func helper = 0;
        
        if (!helper)
            helper = (helper_func)sipImportSymbol("qpycore_qobject_sender");
        
        // sender() must be called without the GIL to avoid possible deadlocks between
        // the GIL ad Qt's internal thread data mutex.
        
        Py_BEGIN_ALLOW_THREADS
        
        #if defined(SIP_PROTECTED_IS_PUBLIC)
        sipRes = sipCpp->sender();
        #else
        sipRes = sipCpp->sipProtect_sender();
        #endif
        
        Py_END_ALLOW_THREADS
        
        if (helper)
            sipRes = helper(sipRes);
#line 327 "./sipCaFrameworkUserMessage.cpp"

            return sipConvertFromType(sipRes,sipType_QObject,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_sender, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_receivers(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_receivers(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        char * a0;
        const sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BG", &sipSelf, sipType_UserMessage, &sipCpp, &a0))
        {
            int sipRes = 0;

#line 672 "C:\\Python26\\sip\\PyQt4/QtCore/qobject.sip"
        // We need to take into account any proxies for Python signals. Import the
        // helper if it hasn't already been done.
        typedef int (*helper_func)(QObject *, const char *, int);
        
        static helper_func helper = 0;
        
        if (!helper)
            helper = (helper_func)sipImportSymbol("qpycore_qobject_receivers");
        
        // PyQt5: Get rid of the const casts.
        if (helper)
        #if defined(SIP_PROTECTED_IS_PUBLIC)
            sipRes = helper(const_cast<QObject *>(sipCpp), a0, sipCpp->receivers(a0));
        #else
            sipRes = helper(const_cast<QObject *>(static_cast<const QObject *>(sipCpp)), a0, sipCpp->sipProtect_receivers(a0));
        #endif
#line 370 "./sipCaFrameworkUserMessage.cpp"

            return SIPLong_FromLong(sipRes);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_receivers, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_timerEvent(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_timerEvent(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        QTimerEvent * a0;
        sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QTimerEvent, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_timerEvent(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_timerEvent, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_childEvent(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_childEvent(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        QChildEvent * a0;
        sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QChildEvent, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_childEvent(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_childEvent, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_customEvent(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_customEvent(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        QEvent * a0;
        sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QEvent, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_customEvent(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_customEvent, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_connectNotify(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_connectNotify(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        char * a0;
        sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BG", &sipSelf, sipType_UserMessage, &sipCpp, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_connectNotify(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_connectNotify, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_disconnectNotify(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_disconnectNotify(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        char * a0;
        sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BG", &sipSelf, sipType_UserMessage, &sipCpp, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_disconnectNotify(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_disconnectNotify, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_senderSignalIndex(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_senderSignalIndex(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const sipUserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_UserMessage, &sipCpp))
        {
            int sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = sipCpp->sipProtect_senderSignalIndex();
            Py_END_ALLOW_THREADS

            return SIPLong_FromLong(sipRes);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_senderSignalIndex, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_setup(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_setup(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        QObject * a0;
        QObject * a1;
        QObject * a2;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8J8J8", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QObject, &a0, sipType_QObject, &a1, sipType_QObject, &a2))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->setup(a0,a1,a2);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    {
        QObject * a0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QObject, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->setup(a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_setup, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_sendStatusMessage(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_sendStatusMessage(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        QString * a0;
        int a0State = 0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QString,&a0, &a0State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sendStatusMessage(*a0);
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    {
        QString * a0;
        int a0State = 0;
        QString * a1;
        int a1State = 0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1J1", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QString,&a0, &a0State, sipType_QString,&a1, &a1State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sendStatusMessage(*a0,*a1);
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);
            sipReleaseType(a1,sipType_QString,a1State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_sendStatusMessage, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_sendWarningMessage(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_sendWarningMessage(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        QString * a0;
        int a0State = 0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QString,&a0, &a0State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sendWarningMessage(*a0);
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    {
        QString * a0;
        int a0State = 0;
        QString * a1;
        int a1State = 0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1J1", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QString,&a0, &a0State, sipType_QString,&a1, &a1State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sendWarningMessage(*a0,*a1);
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);
            sipReleaseType(a1,sipType_QString,a1State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_sendWarningMessage, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_UserMessage_sendErrorMessage(PyObject *, PyObject *);}
static PyObject *meth_UserMessage_sendErrorMessage(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        QString * a0;
        int a0State = 0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QString,&a0, &a0State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sendErrorMessage(*a0);
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    {
        QString * a0;
        int a0State = 0;
        QString * a1;
        int a1State = 0;
        UserMessage *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1J1", &sipSelf, sipType_UserMessage, &sipCpp, sipType_QString,&a0, &a0State, sipType_QString,&a1, &a1State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sendErrorMessage(*a0,*a1);
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);
            sipReleaseType(a1,sipType_QString,a1State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_UserMessage, sipName_sendErrorMessage, NULL);

    return NULL;
}


/* Cast a pointer to a type somewhere in its superclass hierarchy. */
extern "C" {static void *cast_UserMessage(void *, const sipTypeDef *);}
static void *cast_UserMessage(void *ptr, const sipTypeDef *targetType)
{
    void *res;

    if (targetType == sipType_UserMessage)
        return ptr;

    if ((res = ((const sipClassTypeDef *)sipType_QObject)->ctd_cast((QObject *)(UserMessage *)ptr,targetType)) != NULL)
        return res;

    return NULL;
}


/* Call the instance's destructor. */
extern "C" {static void release_UserMessage(void *, int);}
static void release_UserMessage(void *sipCppV,int)
{
    Py_BEGIN_ALLOW_THREADS

    UserMessage *sipCpp = reinterpret_cast<UserMessage *>(sipCppV);

    if (QThread::currentThread() == sipCpp->thread())
        delete sipCpp;
    else
        sipCpp->deleteLater();

    Py_END_ALLOW_THREADS
}


extern "C" {static void dealloc_UserMessage(sipSimpleWrapper *);}
static void dealloc_UserMessage(sipSimpleWrapper *sipSelf)
{
    if (sipIsDerived(sipSelf))
        reinterpret_cast<sipUserMessage *>(sipGetAddress(sipSelf))->sipPySelf = NULL;

    if (sipIsPyOwned(sipSelf))
    {
        release_UserMessage(sipGetAddress(sipSelf),sipSelf->flags);
    }
}


extern "C" {static void *init_UserMessage(sipSimpleWrapper *, PyObject *, PyObject *, PyObject **, PyObject **, PyObject **);}
static void *init_UserMessage(sipSimpleWrapper *sipSelf, PyObject *sipArgs, PyObject *sipKwds, PyObject **sipUnused, PyObject **sipOwner, PyObject **sipParseErr)
{
    sipUserMessage *sipCpp = 0;

    {
        QObject * a0 = 0;

        if (sipParseKwdArgs(sipParseErr, sipArgs, sipKwds, NULL, sipUnused, "|JH", sipType_QObject, &a0, sipOwner))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp = new sipUserMessage(a0);
            Py_END_ALLOW_THREADS

            sipCpp->sipPySelf = sipSelf;

            return sipCpp;
        }
    }

    return NULL;
}


/* Define this type's super-types. */
static sipEncodedTypeDef supers_UserMessage[] = {{143, 0, 1}};


static PyMethodDef methods_UserMessage[] = {
    {SIP_MLNAME_CAST(sipName_childEvent), meth_UserMessage_childEvent, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_connectNotify), meth_UserMessage_connectNotify, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_customEvent), meth_UserMessage_customEvent, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_disconnectNotify), meth_UserMessage_disconnectNotify, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_receivers), meth_UserMessage_receivers, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_sendErrorMessage), meth_UserMessage_sendErrorMessage, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_sendStatusMessage), meth_UserMessage_sendStatusMessage, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_sendWarningMessage), meth_UserMessage_sendWarningMessage, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_sender), meth_UserMessage_sender, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_senderSignalIndex), meth_UserMessage_senderSignalIndex, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_setup), meth_UserMessage_setup, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_timerEvent), meth_UserMessage_timerEvent, METH_VARARGS, NULL}
};


/* Define this type's PyQt4 signals. */
static const pyqt4QtSignal pyqt4_signals_UserMessage[] = {
    {"generalMessage(QString)", 0, 0},
    {"errorMessage(QString)", 0, 0},
    {"warningMessage(QString)", 0, 0},
    {"statusMessage(QString)", 0, 0},
    {0, 0, 0}
};


pyqt4ClassTypeDef sipTypeDef_CaFramework_UserMessage = {
{
    {
        -1,
        0,
        0,
        SIP_TYPE_SCC|SIP_TYPE_CLASS,
        sipNameNr_UserMessage,
        {0}
    },
    {
        sipNameNr_UserMessage,
        {0, 0, 1},
        12, methods_UserMessage,
        0, 0,
        0, 0,
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    },
    0,
    -1,
    -1,
    supers_UserMessage,
    0,
    init_UserMessage,
    0,
    0,
#if PY_MAJOR_VERSION >= 3
    0,
    0,
#else
    0,
    0,
    0,
    0,
#endif
    dealloc_UserMessage,
    0,
    0,
    0,
    release_UserMessage,
    cast_UserMessage,
    0,
    0,
    0
},
    &UserMessage::staticMetaObject,
    0,
    pyqt4_signals_UserMessage
};
