/*
 * Interface wrapper code.
 *
 * Generated by SIP 4.14.6 on Fri Sep 25 15:15:27 2015
 */

#include "sipAPICaFramework.h"




pyqt4ClassTypeDef sipTypeDef_CaFramework_qcaobject = {
{
    {
        -1,
        0,
        0,
        SIP_TYPE_NAMESPACE,
        sipNameNr_qcaobject,
        {0}
    },
    {
        sipNameNr_qcaobject,
        {0, 0, 1},
        0, 0,
        0, 0,
        0, 0,
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    },
    0,
    -1,
    -1,
    0,
    0,
    0,
    0,
    0,
#if PY_MAJOR_VERSION >= 3
    0,
    0,
#else
    0,
    0,
    0,
    0,
#endif
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
},
    0,
    0,
    0
};
