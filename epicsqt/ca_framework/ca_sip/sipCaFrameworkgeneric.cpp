/*
 * Interface wrapper code.
 *
 * Generated by SIP 4.14.6 on Fri Sep 25 15:15:32 2015
 */

#include "sipAPICaFramework.h"

#line 4 "Generic.sip"
#include <Generic.h>
#line 12 "./sipCaFrameworkgeneric.cpp"


static sipEnumMemberDef enummembers_generic[] = {
    {sipName_GENERIC_DOUBLE, generic::GENERIC_DOUBLE, 54},
    {sipName_GENERIC_FLOAT, generic::GENERIC_FLOAT, 54},
    {sipName_GENERIC_LONG, generic::GENERIC_LONG, 54},
    {sipName_GENERIC_SHORT, generic::GENERIC_SHORT, 54},
    {sipName_GENERIC_STRING, generic::GENERIC_STRING, 54},
    {sipName_GENERIC_UNKNOWN, generic::GENERIC_UNKNOWN, 54},
    {sipName_GENERIC_UNSIGNED_CHAR, generic::GENERIC_UNSIGNED_CHAR, 54},
    {sipName_GENERIC_UNSIGNED_LONG, generic::GENERIC_UNSIGNED_LONG, 54},
    {sipName_GENERIC_UNSIGNED_SHORT, generic::GENERIC_UNSIGNED_SHORT, 54},
};


pyqt4ClassTypeDef sipTypeDef_CaFramework_generic = {
{
    {
        -1,
        0,
        0,
        SIP_TYPE_NAMESPACE,
        sipNameNr_generic,
        {0}
    },
    {
        sipNameNr_generic,
        {0, 0, 1},
        0, 0,
        9, enummembers_generic,
        0, 0,
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    },
    0,
    -1,
    -1,
    0,
    0,
    0,
    0,
    0,
#if PY_MAJOR_VERSION >= 3
    0,
    0,
#else
    0,
    0,
    0,
    0,
#endif
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
},
    0,
    0,
    0
};
