/*
 * Interface wrapper code.
 *
 * Generated by SIP 4.14.6 on Fri Sep 25 15:15:28 2015
 */

#include "sipAPICaFramework.h"

#line 7 "qepicspv.sip"
#include <qepicspv.h>
#line 12 "./sipCaFrameworkQEpicsPV.cpp"

#line 36 "C:\\Python26\\sip\\PyQt4/QtCore/qstring.sip"
#include <qstring.h>
#line 16 "./sipCaFrameworkQEpicsPV.cpp"
#line 34 "C:\\Python26\\sip\\PyQt4/QtCore/qobject.sip"
#include <qobject.h>
#line 19 "./sipCaFrameworkQEpicsPV.cpp"
#line 265 "C:\\Python26\\sip\\PyQt4/QtCore/qvariant.sip"
#include <qvariant.h>
#line 22 "./sipCaFrameworkQEpicsPV.cpp"
#line 36 "C:\\Python26\\sip\\PyQt4/QtCore/qstringlist.sip"
#include <qstringlist.h>
#line 25 "./sipCaFrameworkQEpicsPV.cpp"
#line 31 "C:\\Python26\\sip\\PyQt4/QtCore/qcoreevent.sip"
#include <qcoreevent.h>
#line 28 "./sipCaFrameworkQEpicsPV.cpp"
#line 329 "C:\\Python26\\sip\\PyQt4/QtCore/qcoreevent.sip"
#include <qcoreevent.h>
#line 31 "./sipCaFrameworkQEpicsPV.cpp"
#line 312 "C:\\Python26\\sip\\PyQt4/QtCore/qcoreevent.sip"
#include <qcoreevent.h>
#line 34 "./sipCaFrameworkQEpicsPV.cpp"
#line 33 "C:\\Python26\\sip\\PyQt4/QtCore/qlist.sip"
#include <qlist.h>
#line 37 "./sipCaFrameworkQEpicsPV.cpp"
#line 37 "C:\\Python26\\sip\\PyQt4/QtCore/qbytearray.sip"
#include <qbytearray.h>
#line 40 "./sipCaFrameworkQEpicsPV.cpp"
#line 35 "C:\\Python26\\sip\\PyQt4/QtCore/qnamespace.sip"
#include <qnamespace.h>
#line 43 "./sipCaFrameworkQEpicsPV.cpp"
#line 120 "C:\\Python26\\sip\\PyQt4/QtCore/qlist.sip"
#include <qlist.h>
#line 46 "./sipCaFrameworkQEpicsPV.cpp"
#line 31 "C:\\Python26\\sip\\PyQt4/QtCore/qthread.sip"
#include <qthread.h>
#line 49 "./sipCaFrameworkQEpicsPV.cpp"
#line 35 "C:\\Python26\\sip\\PyQt4/QtCore/qregexp.sip"
#include <qregexp.h>
#line 52 "./sipCaFrameworkQEpicsPV.cpp"
#line 31 "C:\\Python26\\sip\\PyQt4/QtCore/qobjectdefs.sip"
#include <qobjectdefs.h>
#line 55 "./sipCaFrameworkQEpicsPV.cpp"


class sipQEpicsPV : public QEpicsPV
{
public:
    sipQEpicsPV(const QString&,QObject *);
    sipQEpicsPV(QObject *);
    virtual ~sipQEpicsPV();

    int qt_metacall(QMetaObject::Call,int,void **);
    void *qt_metacast(const char *);
    const QMetaObject *metaObject() const;

    /*
     * There is a public method for every protected method visible from
     * this class.
     */
    QObject * sipProtect_sender() const;
    int sipProtect_receivers(const char *) const;
    void sipProtectVirt_timerEvent(bool,QTimerEvent *);
    void sipProtectVirt_childEvent(bool,QChildEvent *);
    void sipProtectVirt_customEvent(bool,QEvent *);
    void sipProtectVirt_connectNotify(bool,const char *);
    void sipProtectVirt_disconnectNotify(bool,const char *);
    int sipProtect_senderSignalIndex() const;

    /*
     * There is a protected method for every virtual method visible from
     * this class.
     */
protected:
    bool event(QEvent *);
    bool eventFilter(QObject *,QEvent *);
    void timerEvent(QTimerEvent *);
    void childEvent(QChildEvent *);
    void customEvent(QEvent *);
    void connectNotify(const char *);
    void disconnectNotify(const char *);

public:
    sipSimpleWrapper *sipPySelf;

private:
    sipQEpicsPV(const sipQEpicsPV &);
    sipQEpicsPV &operator = (const sipQEpicsPV &);

    char sipPyMethods[7];
};

sipQEpicsPV::sipQEpicsPV(const QString& a0,QObject *a1): QEpicsPV(a0,a1), sipPySelf(0)
{
    memset(sipPyMethods, 0, sizeof (sipPyMethods));
}

sipQEpicsPV::sipQEpicsPV(QObject *a0): QEpicsPV(a0), sipPySelf(0)
{
    memset(sipPyMethods, 0, sizeof (sipPyMethods));
}

sipQEpicsPV::~sipQEpicsPV()
{
    sipCommonDtor(sipPySelf);
}

const QMetaObject *sipQEpicsPV::metaObject() const
{
    return sip_CaFramework_qt_metaobject(sipPySelf,sipType_QEpicsPV);
}

int sipQEpicsPV::qt_metacall(QMetaObject::Call _c,int _id,void **_a)
{
    _id = QEpicsPV::qt_metacall(_c,_id,_a);

    if (_id >= 0)
        _id = sip_CaFramework_qt_metacall(sipPySelf,sipType_QEpicsPV,_c,_id,_a);

    return _id;
}

void *sipQEpicsPV::qt_metacast(const char *_clname)
{
    return (sip_CaFramework_qt_metacast && sip_CaFramework_qt_metacast(sipPySelf,sipType_QEpicsPV,_clname)) ? this : QEpicsPV::qt_metacast(_clname);
}

bool sipQEpicsPV::event(QEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[0],sipPySelf,NULL,sipName_event);

    if (!sipMeth)
        return QObject::event(a0);

    typedef bool (*sipVH_QtCore_5)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QEvent *);

    return ((sipVH_QtCore_5)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[5]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

bool sipQEpicsPV::eventFilter(QObject *a0,QEvent *a1)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[1],sipPySelf,NULL,sipName_eventFilter);

    if (!sipMeth)
        return QObject::eventFilter(a0,a1);

    typedef bool (*sipVH_QtCore_18)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QObject *,QEvent *);

    return ((sipVH_QtCore_18)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[18]))(sipGILState, 0, sipPySelf, sipMeth, a0, a1);
}

void sipQEpicsPV::timerEvent(QTimerEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[2],sipPySelf,NULL,sipName_timerEvent);

    if (!sipMeth)
    {
        QObject::timerEvent(a0);
        return;
    }

    typedef void (*sipVH_QtCore_9)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QTimerEvent *);

    ((sipVH_QtCore_9)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[9]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipQEpicsPV::childEvent(QChildEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[3],sipPySelf,NULL,sipName_childEvent);

    if (!sipMeth)
    {
        QObject::childEvent(a0);
        return;
    }

    typedef void (*sipVH_QtCore_25)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QChildEvent *);

    ((sipVH_QtCore_25)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[25]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipQEpicsPV::customEvent(QEvent *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[4],sipPySelf,NULL,sipName_customEvent);

    if (!sipMeth)
    {
        QObject::customEvent(a0);
        return;
    }

    typedef void (*sipVH_QtCore_17)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, QEvent *);

    ((sipVH_QtCore_17)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[17]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipQEpicsPV::connectNotify(const char *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[5],sipPySelf,NULL,sipName_connectNotify);

    if (!sipMeth)
    {
        QObject::connectNotify(a0);
        return;
    }

    typedef void (*sipVH_QtCore_24)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, const char *);

    ((sipVH_QtCore_24)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[24]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

void sipQEpicsPV::disconnectNotify(const char *a0)
{
    sip_gilstate_t sipGILState;
    PyObject *sipMeth;

    sipMeth = sipIsPyMethod(&sipGILState,&sipPyMethods[6],sipPySelf,NULL,sipName_disconnectNotify);

    if (!sipMeth)
    {
        QObject::disconnectNotify(a0);
        return;
    }

    typedef void (*sipVH_QtCore_24)(sip_gilstate_t, sipVirtErrorHandlerFunc, sipSimpleWrapper *, PyObject *, const char *);

    ((sipVH_QtCore_24)(sipModuleAPI_CaFramework_QtCore->em_virthandlers[24]))(sipGILState, 0, sipPySelf, sipMeth, a0);
}

QObject * sipQEpicsPV::sipProtect_sender() const
{
    return QObject::sender();
}

int sipQEpicsPV::sipProtect_receivers(const char *a0) const
{
    return QObject::receivers(a0);
}

void sipQEpicsPV::sipProtectVirt_timerEvent(bool sipSelfWasArg,QTimerEvent *a0)
{
    (sipSelfWasArg ? QObject::timerEvent(a0) : timerEvent(a0));
}

void sipQEpicsPV::sipProtectVirt_childEvent(bool sipSelfWasArg,QChildEvent *a0)
{
    (sipSelfWasArg ? QObject::childEvent(a0) : childEvent(a0));
}

void sipQEpicsPV::sipProtectVirt_customEvent(bool sipSelfWasArg,QEvent *a0)
{
    (sipSelfWasArg ? QObject::customEvent(a0) : customEvent(a0));
}

void sipQEpicsPV::sipProtectVirt_connectNotify(bool sipSelfWasArg,const char *a0)
{
    (sipSelfWasArg ? QObject::connectNotify(a0) : connectNotify(a0));
}

void sipQEpicsPV::sipProtectVirt_disconnectNotify(bool sipSelfWasArg,const char *a0)
{
    (sipSelfWasArg ? QObject::disconnectNotify(a0) : disconnectNotify(a0));
}

int sipQEpicsPV::sipProtect_senderSignalIndex() const
{
    return QObject::senderSignalIndex();
}


extern "C" {static PyObject *meth_QEpicsPV_sender(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_sender(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            QObject *sipRes = 0;

#line 646 "C:\\Python26\\sip\\PyQt4/QtCore/qobject.sip"
        typedef QObject *(*helper_func)(QObject *);
        
        static helper_func helper = 0;
        
        if (!helper)
            helper = (helper_func)sipImportSymbol("qpycore_qobject_sender");
        
        // sender() must be called without the GIL to avoid possible deadlocks between
        // the GIL ad Qt's internal thread data mutex.
        
        Py_BEGIN_ALLOW_THREADS
        
        #if defined(SIP_PROTECTED_IS_PUBLIC)
        sipRes = sipCpp->sender();
        #else
        sipRes = sipCpp->sipProtect_sender();
        #endif
        
        Py_END_ALLOW_THREADS
        
        if (helper)
            sipRes = helper(sipRes);
#line 336 "./sipCaFrameworkQEpicsPV.cpp"

            return sipConvertFromType(sipRes,sipType_QObject,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_sender, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_receivers(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_receivers(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        char * a0;
        const sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BG", &sipSelf, sipType_QEpicsPV, &sipCpp, &a0))
        {
            int sipRes = 0;

#line 672 "C:\\Python26\\sip\\PyQt4/QtCore/qobject.sip"
        // We need to take into account any proxies for Python signals. Import the
        // helper if it hasn't already been done.
        typedef int (*helper_func)(QObject *, const char *, int);
        
        static helper_func helper = 0;
        
        if (!helper)
            helper = (helper_func)sipImportSymbol("qpycore_qobject_receivers");
        
        // PyQt5: Get rid of the const casts.
        if (helper)
        #if defined(SIP_PROTECTED_IS_PUBLIC)
            sipRes = helper(const_cast<QObject *>(sipCpp), a0, sipCpp->receivers(a0));
        #else
            sipRes = helper(const_cast<QObject *>(static_cast<const QObject *>(sipCpp)), a0, sipCpp->sipProtect_receivers(a0));
        #endif
#line 379 "./sipCaFrameworkQEpicsPV.cpp"

            return SIPLong_FromLong(sipRes);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_receivers, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_timerEvent(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_timerEvent(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        QTimerEvent * a0;
        sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_QEpicsPV, &sipCpp, sipType_QTimerEvent, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_timerEvent(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_timerEvent, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_childEvent(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_childEvent(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        QChildEvent * a0;
        sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_QEpicsPV, &sipCpp, sipType_QChildEvent, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_childEvent(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_childEvent, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_customEvent(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_customEvent(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        QEvent * a0;
        sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ8", &sipSelf, sipType_QEpicsPV, &sipCpp, sipType_QEvent, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_customEvent(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_customEvent, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_connectNotify(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_connectNotify(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        char * a0;
        sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BG", &sipSelf, sipType_QEpicsPV, &sipCpp, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_connectNotify(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_connectNotify, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_disconnectNotify(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_disconnectNotify(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;
    bool sipSelfWasArg = (!sipSelf || sipIsDerived((sipSimpleWrapper *)sipSelf));

    {
        char * a0;
        sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BG", &sipSelf, sipType_QEpicsPV, &sipCpp, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->sipProtectVirt_disconnectNotify(sipSelfWasArg,a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_disconnectNotify, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_senderSignalIndex(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_senderSignalIndex(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const sipQEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            int sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = sipCpp->sipProtect_senderSignalIndex();
            Py_END_ALLOW_THREADS

            return SIPLong_FromLong(sipRes);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_senderSignalIndex, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_setDebugLevel(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_setDebugLevel(PyObject *, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        uint a0 = 0;

        if (sipParseArgs(&sipParseErr, sipArgs, "|u", &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            QEpicsPV::setDebugLevel(a0);
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_setDebugLevel, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_get(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_get(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            QVariant *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QVariant(sipCpp->get());
            Py_END_ALLOW_THREADS

            return sipConvertFromNewType(sipRes,sipType_QVariant,NULL);
        }
    }

    {
        const QString * a0;
        int a0State = 0;
        int a1 = 1000;

        if (sipParseArgs(&sipParseErr, sipArgs, "J1|i", sipType_QString,&a0, &a0State, &a1))
        {
            QVariant *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QVariant(QEpicsPV::get(*a0,a1));
            Py_END_ALLOW_THREADS
            sipReleaseType(const_cast<QString *>(a0),sipType_QString,a0State);

            return sipConvertFromNewType(sipRes,sipType_QVariant,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_get, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_needUpdated(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_needUpdated(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->needUpdated();
            Py_END_ALLOW_THREADS

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_needUpdated, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_getUpdated(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_getUpdated(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        int a0 = 1000;
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B|i", &sipSelf, sipType_QEpicsPV, &sipCpp, &a0))
        {
            QVariant *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QVariant(sipCpp->getUpdated(a0));
            Py_END_ALLOW_THREADS

            return sipConvertFromNewType(sipRes,sipType_QVariant,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_getUpdated, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_isConnected(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_isConnected(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            bool sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = sipCpp->isConnected();
            Py_END_ALLOW_THREADS

            return PyBool_FromLong(sipRes);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_isConnected, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_getEnum(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_getEnum(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            QStringList *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QStringList(sipCpp->getEnum());
            Py_END_ALLOW_THREADS

            return sipConvertFromNewType(sipRes,sipType_QStringList,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_getEnum, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_pv(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_pv(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B", &sipSelf, sipType_QEpicsPV, &sipCpp))
        {
            QString *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QString(sipCpp->pv());
            Py_END_ALLOW_THREADS

            return sipConvertFromNewType(sipRes,sipType_QString,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_pv, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_getReady(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_getReady(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        int a0 = 1000;
        const QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B|i", &sipSelf, sipType_QEpicsPV, &sipCpp, &a0))
        {
            QVariant *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QVariant(sipCpp->getReady(a0));
            Py_END_ALLOW_THREADS

            return sipConvertFromNewType(sipRes,sipType_QVariant,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_getReady, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_set(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_set(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        QString * a0;
        int a0State = 0;
        const QVariant * a1;
        int a1State = 0;
        int a2 = -1;

        if (sipParseArgs(&sipParseErr, sipArgs, "J1J1|i", sipType_QString,&a0, &a0State, sipType_QVariant,&a1, &a1State, &a2))
        {
            QVariant *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QVariant(QEpicsPV::set(*a0,*a1,a2));
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QString,a0State);
            sipReleaseType(const_cast<QVariant *>(a1),sipType_QVariant,a1State);

            return sipConvertFromNewType(sipRes,sipType_QVariant,NULL);
        }
    }

    {
        QVariant * a0;
        int a0State = 0;
        int a1 = -1;
        QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "BJ1|i", &sipSelf, sipType_QEpicsPV, &sipCpp, sipType_QVariant,&a0, &a0State, &a1))
        {
            QVariant *sipRes;

            Py_BEGIN_ALLOW_THREADS
            sipRes = new QVariant(sipCpp->set(*a0,a1));
            Py_END_ALLOW_THREADS
            sipReleaseType(a0,sipType_QVariant,a0State);

            return sipConvertFromNewType(sipRes,sipType_QVariant,NULL);
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_set, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_setPV(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_setPV(PyObject *sipSelf, PyObject *sipArgs)
{
    PyObject *sipParseErr = NULL;

    {
        const QString& a0def = "";
        const QString * a0 = &a0def;
        int a0State = 0;
        QEpicsPV *sipCpp;

        if (sipParseArgs(&sipParseErr, sipArgs, "B|J1", &sipSelf, sipType_QEpicsPV, &sipCpp, sipType_QString,&a0, &a0State))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp->setPV(*a0);
            Py_END_ALLOW_THREADS
            sipReleaseType(const_cast<QString *>(a0),sipType_QString,a0State);

            Py_INCREF(Py_None);
            return Py_None;
        }
    }

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(sipParseErr, sipName_QEpicsPV, sipName_setPV, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_updateValue(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_updateValue(PyObject *, PyObject *)
{

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(NULL, sipName_QEpicsPV, sipName_updateValue, NULL);

    return NULL;
}


extern "C" {static PyObject *meth_QEpicsPV_updateConnection(PyObject *, PyObject *);}
static PyObject *meth_QEpicsPV_updateConnection(PyObject *, PyObject *)
{

    /* Raise an exception if the arguments couldn't be parsed. */
    sipNoMethod(NULL, sipName_QEpicsPV, sipName_updateConnection, NULL);

    return NULL;
}


/* Cast a pointer to a type somewhere in its superclass hierarchy. */
extern "C" {static void *cast_QEpicsPV(void *, const sipTypeDef *);}
static void *cast_QEpicsPV(void *ptr, const sipTypeDef *targetType)
{
    void *res;

    if (targetType == sipType_QEpicsPV)
        return ptr;

    if ((res = ((const sipClassTypeDef *)sipType_QObject)->ctd_cast((QObject *)(QEpicsPV *)ptr,targetType)) != NULL)
        return res;

    return NULL;
}


/* Call the instance's destructor. */
extern "C" {static void release_QEpicsPV(void *, int);}
static void release_QEpicsPV(void *sipCppV,int)
{
    Py_BEGIN_ALLOW_THREADS

    QEpicsPV *sipCpp = reinterpret_cast<QEpicsPV *>(sipCppV);

    if (QThread::currentThread() == sipCpp->thread())
        delete sipCpp;
    else
        sipCpp->deleteLater();

    Py_END_ALLOW_THREADS
}


extern "C" {static void dealloc_QEpicsPV(sipSimpleWrapper *);}
static void dealloc_QEpicsPV(sipSimpleWrapper *sipSelf)
{
    if (sipIsDerived(sipSelf))
        reinterpret_cast<sipQEpicsPV *>(sipGetAddress(sipSelf))->sipPySelf = NULL;

    if (sipIsPyOwned(sipSelf))
    {
        release_QEpicsPV(sipGetAddress(sipSelf),sipSelf->flags);
    }
}


extern "C" {static void *init_QEpicsPV(sipSimpleWrapper *, PyObject *, PyObject *, PyObject **, PyObject **, PyObject **);}
static void *init_QEpicsPV(sipSimpleWrapper *sipSelf, PyObject *sipArgs, PyObject *sipKwds, PyObject **sipUnused, PyObject **sipOwner, PyObject **sipParseErr)
{
    sipQEpicsPV *sipCpp = 0;

    {
        const QString * a0;
        int a0State = 0;
        QObject * a1 = 0;

        if (sipParseKwdArgs(sipParseErr, sipArgs, sipKwds, NULL, sipUnused, "J1|JH", sipType_QString,&a0, &a0State, sipType_QObject, &a1, sipOwner))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp = new sipQEpicsPV(*a0,a1);
            Py_END_ALLOW_THREADS
            sipReleaseType(const_cast<QString *>(a0),sipType_QString,a0State);

            sipCpp->sipPySelf = sipSelf;

            return sipCpp;
        }
    }

    {
        QObject * a0 = 0;

        if (sipParseKwdArgs(sipParseErr, sipArgs, sipKwds, NULL, sipUnused, "|J8", sipType_QObject, &a0))
        {
            Py_BEGIN_ALLOW_THREADS
            sipCpp = new sipQEpicsPV(a0);
            Py_END_ALLOW_THREADS

            sipCpp->sipPySelf = sipSelf;

            return sipCpp;
        }
    }

    return NULL;
}


/* Define this type's super-types. */
static sipEncodedTypeDef supers_QEpicsPV[] = {{143, 0, 1}};


static PyMethodDef methods_QEpicsPV[] = {
    {SIP_MLNAME_CAST(sipName_childEvent), meth_QEpicsPV_childEvent, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_connectNotify), meth_QEpicsPV_connectNotify, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_customEvent), meth_QEpicsPV_customEvent, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_disconnectNotify), meth_QEpicsPV_disconnectNotify, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_get), meth_QEpicsPV_get, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_getEnum), meth_QEpicsPV_getEnum, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_getReady), meth_QEpicsPV_getReady, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_getUpdated), meth_QEpicsPV_getUpdated, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_isConnected), meth_QEpicsPV_isConnected, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_needUpdated), meth_QEpicsPV_needUpdated, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_pv), meth_QEpicsPV_pv, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_receivers), meth_QEpicsPV_receivers, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_sender), meth_QEpicsPV_sender, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_senderSignalIndex), meth_QEpicsPV_senderSignalIndex, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_set), meth_QEpicsPV_set, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_setDebugLevel), meth_QEpicsPV_setDebugLevel, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_setPV), meth_QEpicsPV_setPV, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_timerEvent), meth_QEpicsPV_timerEvent, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_updateConnection), meth_QEpicsPV_updateConnection, METH_VARARGS, NULL},
    {SIP_MLNAME_CAST(sipName_updateValue), meth_QEpicsPV_updateValue, METH_VARARGS, NULL}
};


/* Define this type's PyQt4 signals. */
static const pyqt4QtSignal pyqt4_signals_QEpicsPV[] = {
    {"valueInited(QVariant)", 0, 0},
    {"valueUpdated(QVariant)", 0, 0},
    {"valueChanged(QVariant)", 0, 0},
    {"disconnected()", 0, 0},
    {"connected()", 0, 0},
    {"connectionChanged(bool)", 0, 0},
    {0, 0, 0}
};


extern "C" {static PyObject *varget_QEpicsPV_defaultDelay(void *, PyObject *, PyObject *);}
static PyObject *varget_QEpicsPV_defaultDelay(void *, PyObject *, PyObject *)
{
    int sipVal;
    sipVal = QEpicsPV::defaultDelay;

    return SIPLong_FromLong(sipVal);
}

sipVariableDef variables_QEpicsPV[] = {
    {ClassVariable, sipName_defaultDelay, (PyMethodDef *)varget_QEpicsPV_defaultDelay, NULL, NULL, NULL},
};


pyqt4ClassTypeDef sipTypeDef_CaFramework_QEpicsPV = {
{
    {
        -1,
        0,
        0,
        SIP_TYPE_SCC|SIP_TYPE_CLASS,
        sipNameNr_QEpicsPV,
        {0}
    },
    {
        sipNameNr_QEpicsPV,
        {0, 0, 1},
        20, methods_QEpicsPV,
        0, 0,
        1, variables_QEpicsPV,
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    },
    0,
    -1,
    -1,
    supers_QEpicsPV,
    0,
    init_QEpicsPV,
    0,
    0,
#if PY_MAJOR_VERSION >= 3
    0,
    0,
#else
    0,
    0,
    0,
    0,
#endif
    dealloc_QEpicsPV,
    0,
    0,
    0,
    release_QEpicsPV,
    cast_QEpicsPV,
    0,
    0,
    0
},
    &QEpicsPV::staticMetaObject,
    0,
    pyqt4_signals_QEpicsPV
};
