from PyQt4 import pyqtconfig

# These are installation specific values created when Ca_framework was configured.
# The following line will be replaced when this template is used to create
# the final configuration module.
_pkg_config = {
    'CaFramework_sip_dir':    'C:\\Python26\\sip',
    'CaFramework_sip_flags':  '-x VendorID -t WS_WIN -x PyQt_OpenSSL -x PyQt_NoPrintRangeBug -t Qt_4_8_0 -x Py_v3 -g'
}

_default_macros = None

class Configuration(pyqtconfig.Configuration):
    """The class that represents CaFramework configuration values.
    """
    def __init__(self, sub_cfg=None):
        """Initialise an instance of the class.

        sub_cfg is the list of sub-class configurations.  It should be None
        when called normally.
        """
        # This is all standard code to be copied verbatim except for the
        # name of the module containing the super-class.
        if sub_cfg:
            cfg = sub_cfg
        else:
            cfg = []

        cfg.append(_pkg_config)

        pyqtconfig.Configuration.__init__(self, cfg)

class CaframeworkModuleMakefile(pyqtconfig.QtGuiModuleMakefile):
    """The Makefile class for modules that %Import CaFramework.
    """
    def finalise(self):
        """Finalise the macros.
        """
        # Make sure our C++ library is linked.
        self.extra_libs.append("CaFramework")
        self.extra_libs.append("QCaPlugin")
        #self.extra_libs.append("ca")
        #self.extra_libs.append("Com")


	  #mais libs para o epics aqui???
        # Let the super-class do what it needs to.
        pyqtconfig.QtGuiModuleMakefile.finalise(self)

