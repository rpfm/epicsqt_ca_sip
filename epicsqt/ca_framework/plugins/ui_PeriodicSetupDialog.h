/********************************************************************************
** Form generated from reading UI file 'PeriodicSetupDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PERIODICSETUPDIALOG_H
#define UI_PERIODICSETUPDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PeriodicSetupDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *periodicGridLayout;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PeriodicSetupDialog)
    {
        if (PeriodicSetupDialog->objectName().isEmpty())
            PeriodicSetupDialog->setObjectName(QString::fromUtf8("PeriodicSetupDialog"));
        PeriodicSetupDialog->resize(1300, 950);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PeriodicSetupDialog->sizePolicy().hasHeightForWidth());
        PeriodicSetupDialog->setSizePolicy(sizePolicy);
        PeriodicSetupDialog->setMinimumSize(QSize(1300, 950));
        verticalLayout = new QVBoxLayout(PeriodicSetupDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        periodicGridLayout = new QGridLayout();
        periodicGridLayout->setObjectName(QString::fromUtf8("periodicGridLayout"));
        periodicGridLayout->setSizeConstraint(QLayout::SetNoConstraint);

        verticalLayout->addLayout(periodicGridLayout);

        buttonBox = new QDialogButtonBox(PeriodicSetupDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(PeriodicSetupDialog);

        QMetaObject::connectSlotsByName(PeriodicSetupDialog);
    } // setupUi

    void retranslateUi(QDialog *PeriodicSetupDialog)
    {
        PeriodicSetupDialog->setWindowTitle(QApplication::translate("PeriodicSetupDialog", "QCaPeriodic - Element configuration", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        PeriodicSetupDialog->setToolTip(QApplication::translate("PeriodicSetupDialog", "Element configuration", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

namespace Ui {
    class PeriodicSetupDialog: public Ui_PeriodicSetupDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PERIODICSETUPDIALOG_H
