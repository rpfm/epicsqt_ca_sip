/****************************************************************************
** Meta object code from reading C++ file 'QCaPeriodicPlugin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../include/QCaPeriodicPlugin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaPeriodicPlugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaPeriodicPlugin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
      18,   19, // properties
       2,   73, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      76,   19,   18,   18, 0x08,

 // properties: name, type, flags
     133,  125, 0x0a095003,
     154,  125, 0x0a095003,
     188,  125, 0x0a095003,
     209,  125, 0x0a095003,
     243,  125, 0x0a095003,
     266,  125, 0x0a095003,
     302,  125, 0x0a095003,
     325,  125, 0x0a095003,
     366,  361, 0x01095103,
     376,  361, 0x01095103,
     394,  361, 0x01095103,
     402,  361, 0x01095103,
     432,  412, 0x0009500b,
     465,  451, 0x0009500b,
     479,  451, 0x0009500b,
     500,  493, 0x06095103,
     519,  493, 0x06095103,
     538,  125, 0x0a095103,

 // enums: name, flags, count, data
     412, 0x0,    3,   81,
     451, 0x0,    9,   87,

 // enum data: key, value
     547, uint(QCaPeriodicPlugin::buttonAndLabel),
     562, uint(QCaPeriodicPlugin::buttonOnly),
     573, uint(QCaPeriodicPlugin::labelOnly),
     583, uint(QCaPeriodicPlugin::Number),
     590, uint(QCaPeriodicPlugin::atomicWeight),
     603, uint(QCaPeriodicPlugin::meltingPoint),
     616, uint(QCaPeriodicPlugin::boilingPoint),
     629, uint(QCaPeriodicPlugin::density),
     637, uint(QCaPeriodicPlugin::group),
     643, uint(QCaPeriodicPlugin::ionizationEnergy),
     660, uint(QCaPeriodicPlugin::userValue1),
     671, uint(QCaPeriodicPlugin::userValue2),

       0        // eod
};

static const char qt_meta_stringdata_QCaPeriodicPlugin[] = {
    "QCaPeriodicPlugin\0\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0writeButtonVariable1\0"
    "writeButtonVariableSubstitutions1\0"
    "writeButtonVariable2\0"
    "writeButtonVariableSubstitutions2\0"
    "readbackLabelVariable1\0"
    "readbackLabelVariableSubstitutions1\0"
    "readbackLabelVariable2\0"
    "readbackLabelVariableSubstitutions2\0"
    "bool\0subscribe\0variableAsToolTip\0"
    "enabled\0allowDrop\0PresentationOptions\0"
    "presentationOption\0VariableTypes\0"
    "variableType1\0variableType2\0double\0"
    "variableTolerance1\0variableTolerance2\0"
    "userInfo\0buttonAndLabel\0buttonOnly\0"
    "labelOnly\0Number\0atomicWeight\0"
    "meltingPoint\0boilingPoint\0density\0"
    "group\0ionizationEnergy\0userValue1\0"
    "userValue2\0"
};

void QCaPeriodicPlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaPeriodicPlugin *_t = static_cast<QCaPeriodicPlugin *>(_o);
        switch (_id) {
        case 0: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaPeriodicPlugin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaPeriodicPlugin::staticMetaObject = {
    { &QCaPeriodic::staticMetaObject, qt_meta_stringdata_QCaPeriodicPlugin,
      qt_meta_data_QCaPeriodicPlugin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaPeriodicPlugin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaPeriodicPlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaPeriodicPlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaPeriodicPlugin))
        return static_cast<void*>(const_cast< QCaPeriodicPlugin*>(this));
    return QCaPeriodic::qt_metacast(_clname);
}

int QCaPeriodicPlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCaPeriodic::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableName1Property(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutions1Property(); break;
        case 2: *reinterpret_cast< QString*>(_v) = getVariableName2Property(); break;
        case 3: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutions2Property(); break;
        case 4: *reinterpret_cast< QString*>(_v) = getVariableName3Property(); break;
        case 5: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutions3Property(); break;
        case 6: *reinterpret_cast< QString*>(_v) = getVariableName4Property(); break;
        case 7: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutions4Property(); break;
        case 8: *reinterpret_cast< bool*>(_v) = getSubscribe(); break;
        case 9: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 10: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 11: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 12: *reinterpret_cast< PresentationOptions*>(_v) = getPresentationOptionProperty(); break;
        case 13: *reinterpret_cast< VariableTypes*>(_v) = getVariableType1Property(); break;
        case 14: *reinterpret_cast< VariableTypes*>(_v) = getVariableType2Property(); break;
        case 15: *reinterpret_cast< double*>(_v) = getVariableTolerance1(); break;
        case 16: *reinterpret_cast< double*>(_v) = getVariableTolerance2(); break;
        case 17: *reinterpret_cast< QString*>(_v) = getUserInfo(); break;
        }
        _id -= 18;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableName1Property(*reinterpret_cast< QString*>(_v)); break;
        case 1: setVariableNameSubstitutions1Property(*reinterpret_cast< QString*>(_v)); break;
        case 2: setVariableName2Property(*reinterpret_cast< QString*>(_v)); break;
        case 3: setVariableNameSubstitutions2Property(*reinterpret_cast< QString*>(_v)); break;
        case 4: setVariableName3Property(*reinterpret_cast< QString*>(_v)); break;
        case 5: setVariableNameSubstitutions3Property(*reinterpret_cast< QString*>(_v)); break;
        case 6: setVariableName4Property(*reinterpret_cast< QString*>(_v)); break;
        case 7: setVariableNameSubstitutions4Property(*reinterpret_cast< QString*>(_v)); break;
        case 8: setSubscribe(*reinterpret_cast< bool*>(_v)); break;
        case 9: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 10: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 11: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 12: setPresentationOptionProperty(*reinterpret_cast< PresentationOptions*>(_v)); break;
        case 13: setVariableType1Property(*reinterpret_cast< VariableTypes*>(_v)); break;
        case 14: setVariableType2Property(*reinterpret_cast< VariableTypes*>(_v)); break;
        case 15: setVariableTolerance1(*reinterpret_cast< double*>(_v)); break;
        case 16: setVariableTolerance2(*reinterpret_cast< double*>(_v)); break;
        case 17: setUserInfo(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 18;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 18;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
