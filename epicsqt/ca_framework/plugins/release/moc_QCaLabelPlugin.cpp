/****************************************************************************
** Meta object code from reading C++ file 'QCaLabelPlugin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../include/QCaLabelPlugin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaLabelPlugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaLabelPlugin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
      26,   19, // properties
       4,   97, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      73,   16,   15,   15, 0x08,

 // properties: name, type, flags
     130,  122, 0x0a095003,
     139,  122, 0x0a095003,
     166,  161, 0x01095103,
     184,  161, 0x01095103,
     192,  161, 0x01095103,
     216,  202, 0x0009500b,
     237,  229, 0x41095003,
     245,  229, 0x41095003,
     253,  229, 0x41095003,
     261,  229, 0x41095003,
     269,  229, 0x41095003,
     277,  229, 0x41095003,
     285,  229, 0x41095003,
     293,  229, 0x41095003,
     306,  301, 0x03095103,
     316,  161, 0x01095103,
     331,  161, 0x01095103,
     343,  161, 0x01095103,
     357,  161, 0x01095103,
     366,  122, 0x0a095103,
     391,  383, 0x0009500b,
     398,  301, 0x03095103,
     414,  404, 0x0009500b,
     436,  423, 0x0009500b,
     448,  301, 0x03095103,
     459,  161, 0x01095003,

 // enums: name, flags, count, data
     202, 0x0,    2,  113,
     383, 0x0,    6,  117,
     404, 0x0,    3,  129,
     423, 0x0,    3,  135,

 // enum data: key, value
     467, uint(QCaLabelPlugin::Text),
     472, uint(QCaLabelPlugin::Picture),
     480, uint(QCaLabelPlugin::Default),
     488, uint(QCaLabelPlugin::Floating),
     497, uint(QCaLabelPlugin::Integer),
     505, uint(QCaLabelPlugin::UnsignedInteger),
     521, uint(QCaLabelPlugin::Time),
     526, uint(QCaLabelPlugin::LocalEnumeration),
     543, uint(QCaLabelPlugin::Fixed),
     549, uint(QCaLabelPlugin::Scientific),
     560, uint(QCaLabelPlugin::Automatic),
     570, uint(QCaLabelPlugin::Append),
     577, uint(QCaLabelPlugin::Ascii),
     583, uint(QCaLabelPlugin::Index),

       0        // eod
};

static const char qt_meta_stringdata_QCaLabelPlugin[] = {
    "QCaLabelPlugin\0\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0variable\0variableSubstitutions\0"
    "bool\0variableAsToolTip\0enabled\0allowDrop\0"
    "UpdateOptions\0updateOption\0QPixmap\0"
    "pixmap0\0pixmap1\0pixmap2\0pixmap3\0pixmap4\0"
    "pixmap5\0pixmap6\0pixmap7\0uint\0precision\0"
    "useDbPrecision\0leadingZero\0trailingZeros\0"
    "addUnits\0localEnumeration\0Formats\0"
    "format\0radix\0Notations\0notation\0"
    "ArrayActions\0arrayAction\0arrayIndex\0"
    "visible\0Text\0Picture\0Default\0Floating\0"
    "Integer\0UnsignedInteger\0Time\0"
    "LocalEnumeration\0Fixed\0Scientific\0"
    "Automatic\0Append\0Ascii\0Index\0"
};

void QCaLabelPlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaLabelPlugin *_t = static_cast<QCaLabelPlugin *>(_o);
        switch (_id) {
        case 0: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaLabelPlugin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaLabelPlugin::staticMetaObject = {
    { &QCaLabel::staticMetaObject, qt_meta_stringdata_QCaLabelPlugin,
      qt_meta_data_QCaLabelPlugin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaLabelPlugin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaLabelPlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaLabelPlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaLabelPlugin))
        return static_cast<void*>(const_cast< QCaLabelPlugin*>(this));
    return QCaLabel::qt_metacast(_clname);
}

int QCaLabelPlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCaLabel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableNameProperty(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutionsProperty(); break;
        case 2: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 3: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 4: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 5: *reinterpret_cast< UpdateOptions*>(_v) = getUpdateOptionProperty(); break;
        case 6: *reinterpret_cast< QPixmap*>(_v) = getPixmap0Property(); break;
        case 7: *reinterpret_cast< QPixmap*>(_v) = getPixmap1Property(); break;
        case 8: *reinterpret_cast< QPixmap*>(_v) = getPixmap2Property(); break;
        case 9: *reinterpret_cast< QPixmap*>(_v) = getPixmap3Property(); break;
        case 10: *reinterpret_cast< QPixmap*>(_v) = getPixmap4Property(); break;
        case 11: *reinterpret_cast< QPixmap*>(_v) = getPixmap5Property(); break;
        case 12: *reinterpret_cast< QPixmap*>(_v) = getPixmap6Property(); break;
        case 13: *reinterpret_cast< QPixmap*>(_v) = getPixmap7Property(); break;
        case 14: *reinterpret_cast< uint*>(_v) = getPrecision(); break;
        case 15: *reinterpret_cast< bool*>(_v) = getUseDbPrecision(); break;
        case 16: *reinterpret_cast< bool*>(_v) = getLeadingZero(); break;
        case 17: *reinterpret_cast< bool*>(_v) = getTrailingZeros(); break;
        case 18: *reinterpret_cast< bool*>(_v) = getAddUnits(); break;
        case 19: *reinterpret_cast< QString*>(_v) = getLocalEnumeration(); break;
        case 20: *reinterpret_cast< Formats*>(_v) = getFormatProperty(); break;
        case 21: *reinterpret_cast< uint*>(_v) = getRadix(); break;
        case 22: *reinterpret_cast< Notations*>(_v) = getNotationProperty(); break;
        case 23: *reinterpret_cast< ArrayActions*>(_v) = getArrayActionProperty(); break;
        case 24: *reinterpret_cast< uint*>(_v) = getArrayIndex(); break;
        case 25: *reinterpret_cast< bool*>(_v) = getRunVisible(); break;
        }
        _id -= 26;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableNameProperty(*reinterpret_cast< QString*>(_v)); break;
        case 1: setVariableNameSubstitutionsProperty(*reinterpret_cast< QString*>(_v)); break;
        case 2: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 3: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 4: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 5: setUpdateOptionProperty(*reinterpret_cast< UpdateOptions*>(_v)); break;
        case 6: setPixmap0Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 7: setPixmap1Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 8: setPixmap2Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 9: setPixmap3Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 10: setPixmap4Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 11: setPixmap5Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 12: setPixmap6Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 13: setPixmap7Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 14: setPrecision(*reinterpret_cast< uint*>(_v)); break;
        case 15: setUseDbPrecision(*reinterpret_cast< bool*>(_v)); break;
        case 16: setLeadingZero(*reinterpret_cast< bool*>(_v)); break;
        case 17: setTrailingZeros(*reinterpret_cast< bool*>(_v)); break;
        case 18: setAddUnits(*reinterpret_cast< bool*>(_v)); break;
        case 19: setLocalEnumeration(*reinterpret_cast< QString*>(_v)); break;
        case 20: setFormatProperty(*reinterpret_cast< Formats*>(_v)); break;
        case 21: setRadix(*reinterpret_cast< uint*>(_v)); break;
        case 22: setNotationProperty(*reinterpret_cast< Notations*>(_v)); break;
        case 23: setArrayActionProperty(*reinterpret_cast< ArrayActions*>(_v)); break;
        case 24: setArrayIndex(*reinterpret_cast< uint*>(_v)); break;
        case 25: setRunVisible(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 26;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 26;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
