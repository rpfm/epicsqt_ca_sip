/****************************************************************************
** Meta object code from reading C++ file 'QCaPushButton.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../widgets/include/QCaPushButton.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaPushButton.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaPushButton[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   15,   14,   14, 0x05,
      66,   43,   14,   14, 0x05,
     109,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     140,  125,   14,   14, 0x08,
     217,  178,   14,   14, 0x08,
     272,   14,   14,   14, 0x08,
     286,   14,   14,   14, 0x08,
     309,  301,   14,   14, 0x08,
     327,   43,   14,   14, 0x0a,
     379,  373,   14,   14, 0x0a,
     408,  400,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QCaPushButton[] = {
    "QCaPushButton\0\0out\0dbValueChanged(QString)\0"
    "guiName,creationOption\0"
    "newGui(QString,ASguiForm::creationOptions)\0"
    "requestResend()\0connectionInfo\0"
    "connectionChanged(QCaConnectionInfo&)\0"
    "text,alarmInfo,timestamp,variableIndex\0"
    "setButtonText(QString,QCaAlarmInfo&,QCaDateTime&,uint)\0"
    "userPressed()\0userReleased()\0checked\0"
    "userClicked(bool)\0"
    "launchGui(QString,ASguiForm::creationOptions)\0"
    "state\0requestEnabled(bool)\0message\0"
    "onGeneralMessage(QString)\0"
};

void QCaPushButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaPushButton *_t = static_cast<QCaPushButton *>(_o);
        switch (_id) {
        case 0: _t->dbValueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->newGui((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< ASguiForm::creationOptions(*)>(_a[2]))); break;
        case 2: _t->requestResend(); break;
        case 3: _t->connectionChanged((*reinterpret_cast< QCaConnectionInfo(*)>(_a[1]))); break;
        case 4: _t->setButtonText((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QCaAlarmInfo(*)>(_a[2])),(*reinterpret_cast< QCaDateTime(*)>(_a[3])),(*reinterpret_cast< const uint(*)>(_a[4]))); break;
        case 5: _t->userPressed(); break;
        case 6: _t->userReleased(); break;
        case 7: _t->userClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->launchGui((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< ASguiForm::creationOptions(*)>(_a[2]))); break;
        case 9: _t->requestEnabled((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 10: _t->onGeneralMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaPushButton::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaPushButton::staticMetaObject = {
    { &QPushButton::staticMetaObject, qt_meta_stringdata_QCaPushButton,
      qt_meta_data_QCaPushButton, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaPushButton::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaPushButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaPushButton::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaPushButton))
        return static_cast<void*>(const_cast< QCaPushButton*>(this));
    if (!strcmp(_clname, "QCaGenericButton"))
        return static_cast< QCaGenericButton*>(const_cast< QCaPushButton*>(this));
    return QPushButton::qt_metacast(_clname);
}

int QCaPushButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPushButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void QCaPushButton::dbValueChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QCaPushButton::newGui(QString _t1, ASguiForm::creationOptions _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QCaPushButton::requestResend()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
