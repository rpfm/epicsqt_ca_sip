/****************************************************************************
** Meta object code from reading C++ file 'QCaPushButtonPlugin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../include/QCaPushButtonPlugin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaPushButtonPlugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaPushButtonPlugin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
      38,   19, // properties
       4,  133, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      78,   21,   20,   20, 0x08,

 // properties: name, type, flags
     135,  127, 0x0a095003,
     144,  127, 0x0a095003,
     164,  127, 0x0a095003,
     191,  186, 0x01095103,
     201,  186, 0x01095103,
     219,  186, 0x01095103,
     227,  186, 0x01095103,
     251,  237, 0x0009500b,
     272,  264, 0x41095003,
     280,  264, 0x41095003,
     288,  264, 0x41095003,
     296,  264, 0x41095003,
     304,  264, 0x41095003,
     312,  264, 0x41095003,
     320,  264, 0x41095003,
     328,  264, 0x41095003,
     341,  336, 0x03095103,
     351,  186, 0x01095103,
     366,  186, 0x01095103,
     378,  186, 0x01095103,
     392,  186, 0x01095103,
     401,  127, 0x0a095103,
     432,  418, 0x0009500b,
     450,  442, 0x0009500b,
     467,  457, 0x0009500b,
     476,  127, 0x0a095103,
     485,  186, 0x01095103,
     498,  186, 0x01095103,
     513,  186, 0x01095103,
     526,  127, 0x0a095103,
     536,  127, 0x0a095103,
     548,  127, 0x0a095103,
     558,  127, 0x0a095103,
     575,  127, 0x0a095003,
     585,  127, 0x0a095103,
     605,  593, 0x0b095103,
     615,  127, 0x0a095003,
     643,  623, 0x0009500b,

 // enums: name, flags, count, data
     237, 0x0,    4,  149,
     442, 0x0,    6,  157,
     457, 0x0,    3,  169,
     623, 0x0,    3,  175,

 // enum data: key, value
     658, uint(QCaPushButtonPlugin::Text),
     663, uint(QCaPushButtonPlugin::Icon),
     668, uint(QCaPushButtonPlugin::TextAndIcon),
     680, uint(QCaPushButtonPlugin::State),
     686, uint(QCaPushButtonPlugin::Default),
     694, uint(QCaPushButtonPlugin::Floating),
     703, uint(QCaPushButtonPlugin::Integer),
     711, uint(QCaPushButtonPlugin::UnsignedInteger),
     727, uint(QCaPushButtonPlugin::Time),
     732, uint(QCaPushButtonPlugin::LocalEnumeration),
     749, uint(QCaPushButtonPlugin::Fixed),
     755, uint(QCaPushButtonPlugin::Scientific),
     766, uint(QCaPushButtonPlugin::Automatic),
     776, uint(QCaPushButtonPlugin::Open),
     781, uint(QCaPushButtonPlugin::NewTab),
     788, uint(QCaPushButtonPlugin::NewWindow),

       0        // eod
};

static const char qt_meta_stringdata_QCaPushButtonPlugin[] = {
    "QCaPushButtonPlugin\0\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0variable\0altReadbackVariable\0"
    "variableSubstitutions\0bool\0subscribe\0"
    "variableAsToolTip\0enabled\0allowDrop\0"
    "UpdateOptions\0updateOption\0QPixmap\0"
    "pixmap0\0pixmap1\0pixmap2\0pixmap3\0pixmap4\0"
    "pixmap5\0pixmap6\0pixmap7\0uint\0precision\0"
    "useDbPrecision\0leadingZero\0trailingZeros\0"
    "addUnits\0localEnumeration\0Qt::Alignment\0"
    "alignment\0Formats\0format\0Notations\0"
    "notation\0password\0writeOnPress\0"
    "writeOnRelease\0writeOnClick\0pressText\0"
    "releaseText\0clickText\0clickCheckedText\0"
    "labelText\0program\0QStringList\0arguments\0"
    "guiFile\0CreationOptionNames\0creationOption\0"
    "Text\0Icon\0TextAndIcon\0State\0Default\0"
    "Floating\0Integer\0UnsignedInteger\0Time\0"
    "LocalEnumeration\0Fixed\0Scientific\0"
    "Automatic\0Open\0NewTab\0NewWindow\0"
};

void QCaPushButtonPlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaPushButtonPlugin *_t = static_cast<QCaPushButtonPlugin *>(_o);
        switch (_id) {
        case 0: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaPushButtonPlugin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaPushButtonPlugin::staticMetaObject = {
    { &QCaPushButton::staticMetaObject, qt_meta_stringdata_QCaPushButtonPlugin,
      qt_meta_data_QCaPushButtonPlugin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaPushButtonPlugin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaPushButtonPlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaPushButtonPlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaPushButtonPlugin))
        return static_cast<void*>(const_cast< QCaPushButtonPlugin*>(this));
    return QCaPushButton::qt_metacast(_clname);
}

int QCaPushButtonPlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCaPushButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableNameProperty(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getAltReadbackVariableNameProperty(); break;
        case 2: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutionsProperty(); break;
        case 3: *reinterpret_cast< bool*>(_v) = getSubscribe(); break;
        case 4: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 5: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 6: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 7: *reinterpret_cast< UpdateOptions*>(_v) = getUpdateOptionProperty(); break;
        case 8: *reinterpret_cast< QPixmap*>(_v) = getPixmap0Property(); break;
        case 9: *reinterpret_cast< QPixmap*>(_v) = getPixmap1Property(); break;
        case 10: *reinterpret_cast< QPixmap*>(_v) = getPixmap2Property(); break;
        case 11: *reinterpret_cast< QPixmap*>(_v) = getPixmap3Property(); break;
        case 12: *reinterpret_cast< QPixmap*>(_v) = getPixmap4Property(); break;
        case 13: *reinterpret_cast< QPixmap*>(_v) = getPixmap5Property(); break;
        case 14: *reinterpret_cast< QPixmap*>(_v) = getPixmap6Property(); break;
        case 15: *reinterpret_cast< QPixmap*>(_v) = getPixmap7Property(); break;
        case 16: *reinterpret_cast< uint*>(_v) = getPrecision(); break;
        case 17: *reinterpret_cast< bool*>(_v) = getUseDbPrecision(); break;
        case 18: *reinterpret_cast< bool*>(_v) = getLeadingZero(); break;
        case 19: *reinterpret_cast< bool*>(_v) = getTrailingZeros(); break;
        case 20: *reinterpret_cast< bool*>(_v) = getAddUnits(); break;
        case 21: *reinterpret_cast< QString*>(_v) = getLocalEnumeration(); break;
        case 22: *reinterpret_cast< Qt::Alignment*>(_v) = getTextAlignment(); break;
        case 23: *reinterpret_cast< Formats*>(_v) = getFormatProperty(); break;
        case 24: *reinterpret_cast< Notations*>(_v) = getNotationProperty(); break;
        case 25: *reinterpret_cast< QString*>(_v) = getPassword(); break;
        case 26: *reinterpret_cast< bool*>(_v) = getWriteOnPress(); break;
        case 27: *reinterpret_cast< bool*>(_v) = getWriteOnRelease(); break;
        case 28: *reinterpret_cast< bool*>(_v) = getWriteOnClick(); break;
        case 29: *reinterpret_cast< QString*>(_v) = getPressText(); break;
        case 30: *reinterpret_cast< QString*>(_v) = getReleaseText(); break;
        case 31: *reinterpret_cast< QString*>(_v) = getClickText(); break;
        case 32: *reinterpret_cast< QString*>(_v) = getClickCheckedText(); break;
        case 33: *reinterpret_cast< QString*>(_v) = getLabelTextProperty(); break;
        case 34: *reinterpret_cast< QString*>(_v) = getProgram(); break;
        case 35: *reinterpret_cast< QStringList*>(_v) = getArguments(); break;
        case 36: *reinterpret_cast< QString*>(_v) = getGuiName(); break;
        case 37: *reinterpret_cast< CreationOptionNames*>(_v) = getCreationOptionProperty(); break;
        }
        _id -= 38;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableNameProperty(*reinterpret_cast< QString*>(_v)); break;
        case 1: setAltReadbackVariableNameProperty(*reinterpret_cast< QString*>(_v)); break;
        case 2: setVariableNameSubstitutionsProperty(*reinterpret_cast< QString*>(_v)); break;
        case 3: setSubscribe(*reinterpret_cast< bool*>(_v)); break;
        case 4: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 5: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 6: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 7: setUpdateOptionProperty(*reinterpret_cast< UpdateOptions*>(_v)); break;
        case 8: setPixmap0Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 9: setPixmap1Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 10: setPixmap2Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 11: setPixmap3Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 12: setPixmap4Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 13: setPixmap5Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 14: setPixmap6Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 15: setPixmap7Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 16: setPrecision(*reinterpret_cast< uint*>(_v)); break;
        case 17: setUseDbPrecision(*reinterpret_cast< bool*>(_v)); break;
        case 18: setLeadingZero(*reinterpret_cast< bool*>(_v)); break;
        case 19: setTrailingZeros(*reinterpret_cast< bool*>(_v)); break;
        case 20: setAddUnits(*reinterpret_cast< bool*>(_v)); break;
        case 21: setLocalEnumeration(*reinterpret_cast< QString*>(_v)); break;
        case 22: setTextAlignment(*reinterpret_cast< Qt::Alignment*>(_v)); break;
        case 23: setFormatProperty(*reinterpret_cast< Formats*>(_v)); break;
        case 24: setNotationProperty(*reinterpret_cast< Notations*>(_v)); break;
        case 25: setPassword(*reinterpret_cast< QString*>(_v)); break;
        case 26: setWriteOnPress(*reinterpret_cast< bool*>(_v)); break;
        case 27: setWriteOnRelease(*reinterpret_cast< bool*>(_v)); break;
        case 28: setWriteOnClick(*reinterpret_cast< bool*>(_v)); break;
        case 29: setPressText(*reinterpret_cast< QString*>(_v)); break;
        case 30: setReleaseText(*reinterpret_cast< QString*>(_v)); break;
        case 31: setClickText(*reinterpret_cast< QString*>(_v)); break;
        case 32: setClickCheckedText(*reinterpret_cast< QString*>(_v)); break;
        case 33: setLabelTextProperty(*reinterpret_cast< QString*>(_v)); break;
        case 34: setProgram(*reinterpret_cast< QString*>(_v)); break;
        case 35: setArguments(*reinterpret_cast< QStringList*>(_v)); break;
        case 36: setGuiName(*reinterpret_cast< QString*>(_v)); break;
        case 37: setCreationOptionProperty(*reinterpret_cast< CreationOptionNames*>(_v)); break;
        }
        _id -= 38;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 38;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 38;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 38;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 38;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 38;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 38;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
