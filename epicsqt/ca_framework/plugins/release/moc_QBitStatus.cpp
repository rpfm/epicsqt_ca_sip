/****************************************************************************
** Meta object code from reading C++ file 'QBitStatus.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../widgets/include/QBitStatus.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QBitStatus.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QBitStatus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
      10,   19, // properties
       1,   49, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   12,   11,   11, 0x0a,

 // properties: name, type, flags
      12,   33, 0x02095103,
      37,   33, 0x02095103,
      50,   33, 0x02095103,
      63,   56, 0x43095003,
      77,   56, 0x43095103,
      91,   56, 0x43095103,
     100,   56, 0x43095103,
     115,  110, 0x01095103,
     126,  110, 0x01095103,
     147,  134, 0x0009510b,

 // enums: name, flags, count, data
     134, 0x0,    4,   53,

 // enum data: key, value
     159, uint(QBitStatus::LSB_On_Right),
     172, uint(QBitStatus::LSB_On_Bottom),
     186, uint(QBitStatus::LSB_On_Left),
     198, uint(QBitStatus::LSB_On_Top),

       0        // eod
};

static const char qt_meta_stringdata_QBitStatus[] = {
    "QBitStatus\0\0value\0setValue(long)\0int\0"
    "numberOfBits\0shift\0QColor\0boarderColour\0"
    "invalidColour\0onColour\0offColour\0bool\0"
    "drawBorder\0isValid\0Orientations\0"
    "Orientation\0LSB_On_Right\0LSB_On_Bottom\0"
    "LSB_On_Left\0LSB_On_Top\0"
};

void QBitStatus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QBitStatus *_t = static_cast<QBitStatus *>(_o);
        switch (_id) {
        case 0: _t->setValue((*reinterpret_cast< const long(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QBitStatus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QBitStatus::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_QBitStatus,
      qt_meta_data_QBitStatus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QBitStatus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QBitStatus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QBitStatus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QBitStatus))
        return static_cast<void*>(const_cast< QBitStatus*>(this));
    return QWidget::qt_metacast(_clname);
}

int QBitStatus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = getValue(); break;
        case 1: *reinterpret_cast< int*>(_v) = getNumberOfBits(); break;
        case 2: *reinterpret_cast< int*>(_v) = getShift(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = getBorderColour(); break;
        case 4: *reinterpret_cast< QColor*>(_v) = getInvalidColour(); break;
        case 5: *reinterpret_cast< QColor*>(_v) = getOnColour(); break;
        case 6: *reinterpret_cast< QColor*>(_v) = getOffColour(); break;
        case 7: *reinterpret_cast< bool*>(_v) = getDrawBorder(); break;
        case 8: *reinterpret_cast< bool*>(_v) = getIsValid(); break;
        case 9: *reinterpret_cast< Orientations*>(_v) = getOrientation(); break;
        }
        _id -= 10;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setValue(*reinterpret_cast< int*>(_v)); break;
        case 1: setNumberOfBits(*reinterpret_cast< int*>(_v)); break;
        case 2: setShift(*reinterpret_cast< int*>(_v)); break;
        case 3: setBorderColour(*reinterpret_cast< QColor*>(_v)); break;
        case 4: setInvalidColour(*reinterpret_cast< QColor*>(_v)); break;
        case 5: setOnColour(*reinterpret_cast< QColor*>(_v)); break;
        case 6: setOffColour(*reinterpret_cast< QColor*>(_v)); break;
        case 7: setDrawBorder(*reinterpret_cast< bool*>(_v)); break;
        case 8: setIsValid(*reinterpret_cast< bool*>(_v)); break;
        case 9: setOrientation(*reinterpret_cast< Orientations*>(_v)); break;
        }
        _id -= 10;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
