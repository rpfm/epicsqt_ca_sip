/****************************************************************************
** Meta object code from reading C++ file 'QCaRadioButtonPlugin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../include/QCaRadioButtonPlugin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaRadioButtonPlugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaRadioButtonPlugin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
      36,   19, // properties
       4,  127, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      79,   22,   21,   21, 0x08,

 // properties: name, type, flags
     136,  128, 0x0a095003,
     145,  128, 0x0a095003,
     172,  167, 0x01095103,
     182,  167, 0x01095103,
     200,  167, 0x01095103,
     208,  167, 0x01095103,
     232,  218, 0x0009500b,
     253,  245, 0x41095003,
     261,  245, 0x41095003,
     269,  245, 0x41095003,
     277,  245, 0x41095003,
     285,  245, 0x41095003,
     293,  245, 0x41095003,
     301,  245, 0x41095003,
     309,  245, 0x41095003,
     322,  317, 0x03095103,
     332,  167, 0x01095103,
     347,  167, 0x01095103,
     359,  167, 0x01095103,
     373,  167, 0x01095103,
     396,  382, 0x0009500b,
     414,  406, 0x0009500b,
     431,  421, 0x0009500b,
     440,  128, 0x0a095103,
     449,  167, 0x01095103,
     462,  167, 0x01095103,
     477,  167, 0x01095103,
     490,  128, 0x0a095103,
     500,  128, 0x0a095103,
     512,  128, 0x0a095103,
     522,  128, 0x0a095103,
     539,  128, 0x0a095003,
     549,  128, 0x0a095103,
     569,  557, 0x0b095103,
     579,  128, 0x0a095003,
     607,  587, 0x0009500b,

 // enums: name, flags, count, data
     218, 0x0,    4,  143,
     406, 0x0,    5,  151,
     421, 0x0,    3,  161,
     587, 0x0,    3,  167,

 // enum data: key, value
     622, uint(QCaRadioButtonPlugin::Text),
     627, uint(QCaRadioButtonPlugin::Icon),
     632, uint(QCaRadioButtonPlugin::TextAndIcon),
     644, uint(QCaRadioButtonPlugin::State),
     650, uint(QCaRadioButtonPlugin::Default),
     658, uint(QCaRadioButtonPlugin::Floating),
     667, uint(QCaRadioButtonPlugin::Integer),
     675, uint(QCaRadioButtonPlugin::UnsignedInteger),
     691, uint(QCaRadioButtonPlugin::Time),
     696, uint(QCaRadioButtonPlugin::Fixed),
     702, uint(QCaRadioButtonPlugin::Scientific),
     713, uint(QCaRadioButtonPlugin::Automatic),
     723, uint(QCaRadioButtonPlugin::Open),
     728, uint(QCaRadioButtonPlugin::NewTab),
     735, uint(QCaRadioButtonPlugin::NewWindow),

       0        // eod
};

static const char qt_meta_stringdata_QCaRadioButtonPlugin[] = {
    "QCaRadioButtonPlugin\0\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0variable\0variableSubstitutions\0"
    "bool\0subscribe\0variableAsToolTip\0"
    "enabled\0allowDrop\0UpdateOptions\0"
    "updateOption\0QPixmap\0pixmap0\0pixmap1\0"
    "pixmap2\0pixmap3\0pixmap4\0pixmap5\0pixmap6\0"
    "pixmap7\0uint\0precision\0useDbPrecision\0"
    "leadingZero\0trailingZeros\0addUnits\0"
    "Qt::Alignment\0alignment\0Formats\0format\0"
    "Notations\0notation\0password\0writeOnPress\0"
    "writeOnRelease\0writeOnClick\0pressText\0"
    "releaseText\0clickText\0clickCheckedText\0"
    "labelText\0program\0QStringList\0arguments\0"
    "guiFile\0CreationOptionNames\0creationOption\0"
    "Text\0Icon\0TextAndIcon\0State\0Default\0"
    "Floating\0Integer\0UnsignedInteger\0Time\0"
    "Fixed\0Scientific\0Automatic\0Open\0NewTab\0"
    "NewWindow\0"
};

void QCaRadioButtonPlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaRadioButtonPlugin *_t = static_cast<QCaRadioButtonPlugin *>(_o);
        switch (_id) {
        case 0: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaRadioButtonPlugin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaRadioButtonPlugin::staticMetaObject = {
    { &QCaRadioButton::staticMetaObject, qt_meta_stringdata_QCaRadioButtonPlugin,
      qt_meta_data_QCaRadioButtonPlugin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaRadioButtonPlugin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaRadioButtonPlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaRadioButtonPlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaRadioButtonPlugin))
        return static_cast<void*>(const_cast< QCaRadioButtonPlugin*>(this));
    return QCaRadioButton::qt_metacast(_clname);
}

int QCaRadioButtonPlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCaRadioButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableNameProperty(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutionsProperty(); break;
        case 2: *reinterpret_cast< bool*>(_v) = getSubscribe(); break;
        case 3: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 4: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 5: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 6: *reinterpret_cast< UpdateOptions*>(_v) = getUpdateOptionProperty(); break;
        case 7: *reinterpret_cast< QPixmap*>(_v) = getPixmap0Property(); break;
        case 8: *reinterpret_cast< QPixmap*>(_v) = getPixmap1Property(); break;
        case 9: *reinterpret_cast< QPixmap*>(_v) = getPixmap2Property(); break;
        case 10: *reinterpret_cast< QPixmap*>(_v) = getPixmap3Property(); break;
        case 11: *reinterpret_cast< QPixmap*>(_v) = getPixmap4Property(); break;
        case 12: *reinterpret_cast< QPixmap*>(_v) = getPixmap5Property(); break;
        case 13: *reinterpret_cast< QPixmap*>(_v) = getPixmap6Property(); break;
        case 14: *reinterpret_cast< QPixmap*>(_v) = getPixmap7Property(); break;
        case 15: *reinterpret_cast< uint*>(_v) = getPrecision(); break;
        case 16: *reinterpret_cast< bool*>(_v) = getUseDbPrecision(); break;
        case 17: *reinterpret_cast< bool*>(_v) = getLeadingZero(); break;
        case 18: *reinterpret_cast< bool*>(_v) = getTrailingZeros(); break;
        case 19: *reinterpret_cast< bool*>(_v) = getAddUnits(); break;
        case 20: *reinterpret_cast< Qt::Alignment*>(_v) = getTextAlignment(); break;
        case 21: *reinterpret_cast< Formats*>(_v) = getFormatProperty(); break;
        case 22: *reinterpret_cast< Notations*>(_v) = getNotationProperty(); break;
        case 23: *reinterpret_cast< QString*>(_v) = getPassword(); break;
        case 24: *reinterpret_cast< bool*>(_v) = getWriteOnPress(); break;
        case 25: *reinterpret_cast< bool*>(_v) = getWriteOnRelease(); break;
        case 26: *reinterpret_cast< bool*>(_v) = getWriteOnClick(); break;
        case 27: *reinterpret_cast< QString*>(_v) = getPressText(); break;
        case 28: *reinterpret_cast< QString*>(_v) = getReleaseText(); break;
        case 29: *reinterpret_cast< QString*>(_v) = getClickText(); break;
        case 30: *reinterpret_cast< QString*>(_v) = getClickCheckedText(); break;
        case 31: *reinterpret_cast< QString*>(_v) = getLabelTextProperty(); break;
        case 32: *reinterpret_cast< QString*>(_v) = getProgram(); break;
        case 33: *reinterpret_cast< QStringList*>(_v) = getArguments(); break;
        case 34: *reinterpret_cast< QString*>(_v) = getGuiName(); break;
        case 35: *reinterpret_cast< CreationOptionNames*>(_v) = getCreationOptionProperty(); break;
        }
        _id -= 36;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableNameProperty(*reinterpret_cast< QString*>(_v)); break;
        case 1: setVariableNameSubstitutionsProperty(*reinterpret_cast< QString*>(_v)); break;
        case 2: setSubscribe(*reinterpret_cast< bool*>(_v)); break;
        case 3: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 4: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 5: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 6: setUpdateOptionProperty(*reinterpret_cast< UpdateOptions*>(_v)); break;
        case 7: setPixmap0Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 8: setPixmap1Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 9: setPixmap2Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 10: setPixmap3Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 11: setPixmap4Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 12: setPixmap5Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 13: setPixmap6Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 14: setPixmap7Property(*reinterpret_cast< QPixmap*>(_v)); break;
        case 15: setPrecision(*reinterpret_cast< uint*>(_v)); break;
        case 16: setUseDbPrecision(*reinterpret_cast< bool*>(_v)); break;
        case 17: setLeadingZero(*reinterpret_cast< bool*>(_v)); break;
        case 18: setTrailingZeros(*reinterpret_cast< bool*>(_v)); break;
        case 19: setAddUnits(*reinterpret_cast< bool*>(_v)); break;
        case 20: setTextAlignment(*reinterpret_cast< Qt::Alignment*>(_v)); break;
        case 21: setFormatProperty(*reinterpret_cast< Formats*>(_v)); break;
        case 22: setNotationProperty(*reinterpret_cast< Notations*>(_v)); break;
        case 23: setPassword(*reinterpret_cast< QString*>(_v)); break;
        case 24: setWriteOnPress(*reinterpret_cast< bool*>(_v)); break;
        case 25: setWriteOnRelease(*reinterpret_cast< bool*>(_v)); break;
        case 26: setWriteOnClick(*reinterpret_cast< bool*>(_v)); break;
        case 27: setPressText(*reinterpret_cast< QString*>(_v)); break;
        case 28: setReleaseText(*reinterpret_cast< QString*>(_v)); break;
        case 29: setClickText(*reinterpret_cast< QString*>(_v)); break;
        case 30: setClickCheckedText(*reinterpret_cast< QString*>(_v)); break;
        case 31: setLabelTextProperty(*reinterpret_cast< QString*>(_v)); break;
        case 32: setProgram(*reinterpret_cast< QString*>(_v)); break;
        case 33: setArguments(*reinterpret_cast< QStringList*>(_v)); break;
        case 34: setGuiName(*reinterpret_cast< QString*>(_v)); break;
        case 35: setCreationOptionProperty(*reinterpret_cast< CreationOptionNames*>(_v)); break;
        }
        _id -= 36;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 36;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 36;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 36;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 36;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 36;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 36;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
