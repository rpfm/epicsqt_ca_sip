/****************************************************************************
** Meta object code from reading C++ file 'qepicspv.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../data/include/qepicspv.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qepicspv.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QEpicsPV[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   10,    9,    9, 0x05,
      44,    9,    9,    9, 0x05,
      56,    9,    9,    9, 0x05,
      77,   71,    9,    9, 0x05,
     100,   71,    9,    9, 0x05,
     123,   71,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
     157,  145,    9,    9, 0x0a,
     175,   71,    9,    9, 0x2a,
     197,  189,    9,    9, 0x0a,
     212,    9,    9,    9, 0x2a,
     225,  220,    9,    9, 0x08,
     247,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_QEpicsPV[] = {
    "QEpicsPV\0\0connected\0connectionChanged(bool)\0"
    "connected()\0disconnected()\0value\0"
    "valueChanged(QVariant)\0valueUpdated(QVariant)\0"
    "valueInited(QVariant)\0value,delay\0"
    "set(QVariant,int)\0set(QVariant)\0_pvName\0"
    "setPV(QString)\0setPV()\0data\0"
    "updateValue(QVariant)\0updateConnection()\0"
};

void QEpicsPV::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QEpicsPV *_t = static_cast<QEpicsPV *>(_o);
        switch (_id) {
        case 0: _t->connectionChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->connected(); break;
        case 2: _t->disconnected(); break;
        case 3: _t->valueChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 4: _t->valueUpdated((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 5: _t->valueInited((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 6: _t->set((*reinterpret_cast< QVariant(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->set((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 8: _t->setPV((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->setPV(); break;
        case 10: _t->updateValue((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 11: _t->updateConnection(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QEpicsPV::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QEpicsPV::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QEpicsPV,
      qt_meta_data_QEpicsPV, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QEpicsPV::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QEpicsPV::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QEpicsPV::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QEpicsPV))
        return static_cast<void*>(const_cast< QEpicsPV*>(this));
    return QObject::qt_metacast(_clname);
}

int QEpicsPV::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void QEpicsPV::connectionChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QEpicsPV::connected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QEpicsPV::disconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QEpicsPV::valueChanged(const QVariant & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QEpicsPV::valueUpdated(const QVariant & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QEpicsPV::valueInited(const QVariant & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
