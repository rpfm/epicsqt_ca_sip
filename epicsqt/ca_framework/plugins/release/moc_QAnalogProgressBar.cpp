/****************************************************************************
** Meta object code from reading C++ file 'QAnalogProgressBar.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../widgets/include/QAnalogProgressBar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QAnalogProgressBar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QAnalogProgressBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       3,   34, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      52,   20,   19,   19, 0x0a,
      98,   82,   19,   19, 0x0a,
     139,  123,   19,   19, 0x0a,
     178,  164,   19,   19, 0x0a,

 // properties: name, type, flags
     208,  201, 0x06095103,
     222,  201, 0x06095103,
     236,  201, 0x06095103,

       0        // eod
};

static const char qt_meta_stringdata_QAnalogProgressBar[] = {
    "QAnalogProgressBar\0\0analogMinimumIn,analogMaximumIn\0"
    "setAnalogRange(double,double)\0"
    "analogMinimumIn\0setAnalogMinimum(double)\0"
    "analogMaximumIn\0setAnalogMaximum(double)\0"
    "analogValueIn\0setAnalogValue(double)\0"
    "double\0analogMinimum\0analogMaximum\0"
    "analogValue\0"
};

void QAnalogProgressBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QAnalogProgressBar *_t = static_cast<QAnalogProgressBar *>(_o);
        switch (_id) {
        case 0: _t->setAnalogRange((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2]))); break;
        case 1: _t->setAnalogMinimum((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 2: _t->setAnalogMaximum((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 3: _t->setAnalogValue((*reinterpret_cast< const double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QAnalogProgressBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QAnalogProgressBar::staticMetaObject = {
    { &QProgressBar::staticMetaObject, qt_meta_stringdata_QAnalogProgressBar,
      qt_meta_data_QAnalogProgressBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QAnalogProgressBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QAnalogProgressBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QAnalogProgressBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QAnalogProgressBar))
        return static_cast<void*>(const_cast< QAnalogProgressBar*>(this));
    return QProgressBar::qt_metacast(_clname);
}

int QAnalogProgressBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QProgressBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< double*>(_v) = getAnalogMinimum(); break;
        case 1: *reinterpret_cast< double*>(_v) = getAnalogMaximum(); break;
        case 2: *reinterpret_cast< double*>(_v) = getAnalogValue(); break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setAnalogMinimum(*reinterpret_cast< double*>(_v)); break;
        case 1: setAnalogMaximum(*reinterpret_cast< double*>(_v)); break;
        case 2: setAnalogValue(*reinterpret_cast< double*>(_v)); break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
