/****************************************************************************
** Meta object code from reading C++ file 'QCaAnalogProgressBar.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../widgets/include/QCaAnalogProgressBar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaAnalogProgressBar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaAnalogProgressBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       7,   39, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   22,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
      55,   49,   21,   21, 0x0a,
      91,   76,   21,   21, 0x08,
     138,  129,   21,   21, 0x08,
     255,  198,   21,   21, 0x08,

 // properties: name, type, flags
     312,  304, 0x0a095003,
     321,  304, 0x0a095003,
     348,  343, 0x01095103,
     366,  343, 0x01095103,
     374,  343, 0x01095103,
     384,  343, 0x01095103,
     403,  343, 0x01095003,

       0        // eod
};

static const char qt_meta_stringdata_QCaAnalogProgressBar[] = {
    "QCaAnalogProgressBar\0\0out\0"
    "dbValueChanged(double)\0state\0"
    "requestEnabled(bool)\0connectionInfo\0"
    "connectionChanged(QCaConnectionInfo&)\0"
    "value,,,\0"
    "setProgressBarValue(double,QCaAlarmInfo&,QCaDateTime&,uint)\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0variable\0variableSubstitutions\0"
    "bool\0variableAsToolTip\0enabled\0allowDrop\0"
    "useDbDisplayLimits\0visible\0"
};

void QCaAnalogProgressBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaAnalogProgressBar *_t = static_cast<QCaAnalogProgressBar *>(_o);
        switch (_id) {
        case 0: _t->dbValueChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 1: _t->requestEnabled((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 2: _t->connectionChanged((*reinterpret_cast< QCaConnectionInfo(*)>(_a[1]))); break;
        case 3: _t->setProgressBarValue((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< QCaAlarmInfo(*)>(_a[2])),(*reinterpret_cast< QCaDateTime(*)>(_a[3])),(*reinterpret_cast< const uint(*)>(_a[4]))); break;
        case 4: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaAnalogProgressBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaAnalogProgressBar::staticMetaObject = {
    { &QAnalogProgressBar::staticMetaObject, qt_meta_stringdata_QCaAnalogProgressBar,
      qt_meta_data_QCaAnalogProgressBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaAnalogProgressBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaAnalogProgressBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaAnalogProgressBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaAnalogProgressBar))
        return static_cast<void*>(const_cast< QCaAnalogProgressBar*>(this));
    if (!strcmp(_clname, "QCaWidget"))
        return static_cast< QCaWidget*>(const_cast< QCaAnalogProgressBar*>(this));
    return QAnalogProgressBar::qt_metacast(_clname);
}

int QCaAnalogProgressBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAnalogProgressBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableNameProperty(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutionsProperty(); break;
        case 2: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 3: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 4: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 5: *reinterpret_cast< bool*>(_v) = getUseDbDisplayLimits(); break;
        case 6: *reinterpret_cast< bool*>(_v) = getRunVisible(); break;
        }
        _id -= 7;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableNameProperty(*reinterpret_cast< QString*>(_v)); break;
        case 1: setVariableNameSubstitutionsProperty(*reinterpret_cast< QString*>(_v)); break;
        case 2: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 3: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 4: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 5: setUseDbDisplayLimits(*reinterpret_cast< bool*>(_v)); break;
        case 6: setRunVisible(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 7;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QCaAnalogProgressBar::dbValueChanged(const double & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
