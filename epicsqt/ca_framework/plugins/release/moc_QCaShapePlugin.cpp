/****************************************************************************
** Meta object code from reading C++ file 'QCaShapePlugin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../include/QCaShapePlugin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaShapePlugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaShapePlugin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
      60,   19, // properties
       2,  199, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      73,   16,   15,   15, 0x08,

 // properties: name, type, flags
     130,  122, 0x0a095003,
     140,  122, 0x0a095003,
     150,  122, 0x0a095003,
     160,  122, 0x0a095003,
     170,  122, 0x0a095003,
     180,  122, 0x0a095003,
     195,  190, 0x01095103,
     222,  205, 0x0009500b,
     233,  205, 0x0009500b,
     244,  205, 0x0009500b,
     255,  205, 0x0009500b,
     266,  205, 0x0009500b,
     277,  205, 0x0009500b,
     295,  288, 0x06095003,
     302,  288, 0x06095003,
     309,  288, 0x06095003,
     316,  288, 0x06095003,
     323,  288, 0x06095003,
     330,  288, 0x06095003,
     337,  288, 0x06095003,
     345,  288, 0x06095003,
     353,  288, 0x06095003,
     361,  288, 0x06095003,
     369,  288, 0x06095003,
     377,  288, 0x06095003,
     385,  190, 0x01095103,
     392,  190, 0x01095103,
     397,  122, 0x0a095003,
     419,  190, 0x01095103,
     429,  190, 0x01095103,
     447,  190, 0x01095103,
     468,  455, 0x0009500b,
     479,  474, 0x03095103,
     496,  489, 0x19095103,
     514,  489, 0x19095003,
     521,  489, 0x19095003,
     528,  489, 0x19095003,
     535,  489, 0x19095003,
     542,  489, 0x19095003,
     549,  489, 0x19095003,
     556,  489, 0x19095003,
     563,  489, 0x19095003,
     570,  489, 0x19095003,
     577,  489, 0x19095003,
     592,  585, 0x43095003,
     599,  585, 0x43095003,
     606,  585, 0x43095003,
     613,  585, 0x43095003,
     620,  585, 0x43095003,
     627,  585, 0x43095003,
     634,  585, 0x43095003,
     641,  585, 0x43095003,
     648,  585, 0x43095003,
     655,  585, 0x43095003,
     663,  190, 0x01095103,
     674,  474, 0x03095103,
     684,  288, 0x06095103,
     695,  288, 0x06095103,
     704,  288, 0x06095103,
     714,  122, 0x0a095103,

 // enums: name, flags, count, data
     205, 0x0,   11,  207,
     455, 0x0,   13,  229,

 // enum data: key, value
     719, uint(QCaShapePlugin::Width),
     725, uint(QCaShapePlugin::Height),
     732, uint(QCaShapePlugin::X),
     734, uint(QCaShapePlugin::Y),
     736, uint(QCaShapePlugin::Transperency),
     749, uint(QCaShapePlugin::Rotation),
     758, uint(QCaShapePlugin::ColourHue),
     768, uint(QCaShapePlugin::ColourSaturation),
     785, uint(QCaShapePlugin::ColourValue),
     797, uint(QCaShapePlugin::ColourIndex),
     809, uint(QCaShapePlugin::Penwidth),
     818, uint(QCaShapePlugin::Line),
     823, uint(QCaShapePlugin::Points),
     830, uint(QCaShapePlugin::Polyline),
     839, uint(QCaShapePlugin::Polygon),
     847, uint(QCaShapePlugin::Rect),
     852, uint(QCaShapePlugin::RoundedRect),
     864, uint(QCaShapePlugin::Ellipse),
     872, uint(QCaShapePlugin::Arc),
     876, uint(QCaShapePlugin::Chord),
     882, uint(QCaShapePlugin::Pie),
     886, uint(QCaShapePlugin::Path),
     891, uint(QCaShapePlugin::Text),
     896, uint(QCaShapePlugin::Pixmap),

       0        // eod
};

static const char qt_meta_stringdata_QCaShapePlugin[] = {
    "QCaShapePlugin\0\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0variable1\0variable2\0variable3\0"
    "variable4\0variable5\0variable6\0bool\0"
    "allowDrop\0animationOptions\0animation1\0"
    "animation2\0animation3\0animation4\0"
    "animation5\0animation6\0double\0scale1\0"
    "scale2\0scale3\0scale4\0scale5\0scale6\0"
    "offset1\0offset2\0offset3\0offset4\0offset5\0"
    "offset6\0border\0fill\0variableSubstitutions\0"
    "subscribe\0variableAsToolTip\0enabled\0"
    "shapeOptions\0shape\0uint\0numPoints\0"
    "QPoint\0originTranslation\0point1\0point2\0"
    "point3\0point4\0point5\0point6\0point7\0"
    "point8\0point9\0point10\0QColor\0color1\0"
    "color2\0color3\0color4\0color5\0color6\0"
    "color7\0color8\0color9\0color10\0drawBorder\0"
    "lineWidth\0startAngle\0rotation\0arcLength\0"
    "text\0Width\0Height\0X\0Y\0Transperency\0"
    "Rotation\0ColourHue\0ColourSaturation\0"
    "ColourValue\0ColourIndex\0Penwidth\0Line\0"
    "Points\0Polyline\0Polygon\0Rect\0RoundedRect\0"
    "Ellipse\0Arc\0Chord\0Pie\0Path\0Text\0Pixmap\0"
};

void QCaShapePlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaShapePlugin *_t = static_cast<QCaShapePlugin *>(_o);
        switch (_id) {
        case 0: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaShapePlugin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaShapePlugin::staticMetaObject = {
    { &QCaShape::staticMetaObject, qt_meta_stringdata_QCaShapePlugin,
      qt_meta_data_QCaShapePlugin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaShapePlugin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaShapePlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaShapePlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaShapePlugin))
        return static_cast<void*>(const_cast< QCaShapePlugin*>(this));
    return QCaShape::qt_metacast(_clname);
}

int QCaShapePlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCaShape::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableName1Property(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getVariableName2Property(); break;
        case 2: *reinterpret_cast< QString*>(_v) = getVariableName3Property(); break;
        case 3: *reinterpret_cast< QString*>(_v) = getVariableName4Property(); break;
        case 4: *reinterpret_cast< QString*>(_v) = getVariableName5Property(); break;
        case 5: *reinterpret_cast< QString*>(_v) = getVariableName6Property(); break;
        case 6: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 7: *reinterpret_cast< animationOptions*>(_v) = getAnimation1Property(); break;
        case 8: *reinterpret_cast< animationOptions*>(_v) = getAnimation2Property(); break;
        case 9: *reinterpret_cast< animationOptions*>(_v) = getAnimation3Property(); break;
        case 10: *reinterpret_cast< animationOptions*>(_v) = getAnimation4Property(); break;
        case 11: *reinterpret_cast< animationOptions*>(_v) = getAnimation5Property(); break;
        case 12: *reinterpret_cast< animationOptions*>(_v) = getAnimation6Property(); break;
        case 13: *reinterpret_cast< double*>(_v) = getScale1Property(); break;
        case 14: *reinterpret_cast< double*>(_v) = getScale2Property(); break;
        case 15: *reinterpret_cast< double*>(_v) = getScale3Property(); break;
        case 16: *reinterpret_cast< double*>(_v) = getScale4Property(); break;
        case 17: *reinterpret_cast< double*>(_v) = getScale5Property(); break;
        case 18: *reinterpret_cast< double*>(_v) = getScale6Property(); break;
        case 19: *reinterpret_cast< double*>(_v) = getOffset1Property(); break;
        case 20: *reinterpret_cast< double*>(_v) = getOffset2Property(); break;
        case 21: *reinterpret_cast< double*>(_v) = getOffset3Property(); break;
        case 22: *reinterpret_cast< double*>(_v) = getOffset4Property(); break;
        case 23: *reinterpret_cast< double*>(_v) = getOffset5Property(); break;
        case 24: *reinterpret_cast< double*>(_v) = getOffset6Property(); break;
        case 25: *reinterpret_cast< bool*>(_v) = getBorder(); break;
        case 26: *reinterpret_cast< bool*>(_v) = getFill(); break;
        case 27: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutionsProperty(); break;
        case 28: *reinterpret_cast< bool*>(_v) = getSubscribe(); break;
        case 29: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 30: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 31: *reinterpret_cast< shapeOptions*>(_v) = getShapeProperty(); break;
        case 32: *reinterpret_cast< uint*>(_v) = getNumPoints(); break;
        case 33: *reinterpret_cast< QPoint*>(_v) = getOriginTranslation(); break;
        case 34: *reinterpret_cast< QPoint*>(_v) = getPoint1Property(); break;
        case 35: *reinterpret_cast< QPoint*>(_v) = getPoint2Property(); break;
        case 36: *reinterpret_cast< QPoint*>(_v) = getPoint3Property(); break;
        case 37: *reinterpret_cast< QPoint*>(_v) = getPoint4Property(); break;
        case 38: *reinterpret_cast< QPoint*>(_v) = getPoint5Property(); break;
        case 39: *reinterpret_cast< QPoint*>(_v) = getPoint6Property(); break;
        case 40: *reinterpret_cast< QPoint*>(_v) = getPoint7Property(); break;
        case 41: *reinterpret_cast< QPoint*>(_v) = getPoint8Property(); break;
        case 42: *reinterpret_cast< QPoint*>(_v) = getPoint9Property(); break;
        case 43: *reinterpret_cast< QPoint*>(_v) = getPoint10Property(); break;
        case 44: *reinterpret_cast< QColor*>(_v) = getColor1Property(); break;
        case 45: *reinterpret_cast< QColor*>(_v) = getColor2Property(); break;
        case 46: *reinterpret_cast< QColor*>(_v) = getColor3Property(); break;
        case 47: *reinterpret_cast< QColor*>(_v) = getColor4Property(); break;
        case 48: *reinterpret_cast< QColor*>(_v) = getColor5Property(); break;
        case 49: *reinterpret_cast< QColor*>(_v) = getColor6Property(); break;
        case 50: *reinterpret_cast< QColor*>(_v) = getColor7Property(); break;
        case 51: *reinterpret_cast< QColor*>(_v) = getColor8Property(); break;
        case 52: *reinterpret_cast< QColor*>(_v) = getColor9Property(); break;
        case 53: *reinterpret_cast< QColor*>(_v) = getColor10Property(); break;
        case 54: *reinterpret_cast< bool*>(_v) = getDrawBorder(); break;
        case 55: *reinterpret_cast< uint*>(_v) = getLineWidth(); break;
        case 56: *reinterpret_cast< double*>(_v) = getStartAngle(); break;
        case 57: *reinterpret_cast< double*>(_v) = getRotation(); break;
        case 58: *reinterpret_cast< double*>(_v) = getArcLength(); break;
        case 59: *reinterpret_cast< QString*>(_v) = getText(); break;
        }
        _id -= 60;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableName1Property(*reinterpret_cast< QString*>(_v)); break;
        case 1: setVariableName2Property(*reinterpret_cast< QString*>(_v)); break;
        case 2: setVariableName3Property(*reinterpret_cast< QString*>(_v)); break;
        case 3: setVariableName4Property(*reinterpret_cast< QString*>(_v)); break;
        case 4: setVariableName5Property(*reinterpret_cast< QString*>(_v)); break;
        case 5: setVariableName6Property(*reinterpret_cast< QString*>(_v)); break;
        case 6: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 7: setAnimation1Property(*reinterpret_cast< animationOptions*>(_v)); break;
        case 8: setAnimation2Property(*reinterpret_cast< animationOptions*>(_v)); break;
        case 9: setAnimation3Property(*reinterpret_cast< animationOptions*>(_v)); break;
        case 10: setAnimation4Property(*reinterpret_cast< animationOptions*>(_v)); break;
        case 11: setAnimation5Property(*reinterpret_cast< animationOptions*>(_v)); break;
        case 12: setAnimation6Property(*reinterpret_cast< animationOptions*>(_v)); break;
        case 13: setScale1Property(*reinterpret_cast< double*>(_v)); break;
        case 14: setScale2Property(*reinterpret_cast< double*>(_v)); break;
        case 15: setScale3Property(*reinterpret_cast< double*>(_v)); break;
        case 16: setScale4Property(*reinterpret_cast< double*>(_v)); break;
        case 17: setScale5Property(*reinterpret_cast< double*>(_v)); break;
        case 18: setScale6Property(*reinterpret_cast< double*>(_v)); break;
        case 19: setOffset1Property(*reinterpret_cast< double*>(_v)); break;
        case 20: setOffset2Property(*reinterpret_cast< double*>(_v)); break;
        case 21: setOffset3Property(*reinterpret_cast< double*>(_v)); break;
        case 22: setOffset4Property(*reinterpret_cast< double*>(_v)); break;
        case 23: setOffset5Property(*reinterpret_cast< double*>(_v)); break;
        case 24: setOffset6Property(*reinterpret_cast< double*>(_v)); break;
        case 25: setBorder(*reinterpret_cast< bool*>(_v)); break;
        case 26: setFill(*reinterpret_cast< bool*>(_v)); break;
        case 27: setVariableNameSubstitutionsProperty(*reinterpret_cast< QString*>(_v)); break;
        case 28: setSubscribe(*reinterpret_cast< bool*>(_v)); break;
        case 29: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 30: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 31: setShapeProperty(*reinterpret_cast< shapeOptions*>(_v)); break;
        case 32: setNumPoints(*reinterpret_cast< uint*>(_v)); break;
        case 33: setOriginTranslation(*reinterpret_cast< QPoint*>(_v)); break;
        case 34: setPoint1Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 35: setPoint2Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 36: setPoint3Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 37: setPoint4Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 38: setPoint5Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 39: setPoint6Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 40: setPoint7Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 41: setPoint8Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 42: setPoint9Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 43: setPoint10Property(*reinterpret_cast< QPoint*>(_v)); break;
        case 44: setColor1Property(*reinterpret_cast< QColor*>(_v)); break;
        case 45: setColor2Property(*reinterpret_cast< QColor*>(_v)); break;
        case 46: setColor3Property(*reinterpret_cast< QColor*>(_v)); break;
        case 47: setColor4Property(*reinterpret_cast< QColor*>(_v)); break;
        case 48: setColor5Property(*reinterpret_cast< QColor*>(_v)); break;
        case 49: setColor6Property(*reinterpret_cast< QColor*>(_v)); break;
        case 50: setColor7Property(*reinterpret_cast< QColor*>(_v)); break;
        case 51: setColor8Property(*reinterpret_cast< QColor*>(_v)); break;
        case 52: setColor9Property(*reinterpret_cast< QColor*>(_v)); break;
        case 53: setColor10Property(*reinterpret_cast< QColor*>(_v)); break;
        case 54: setDrawBorder(*reinterpret_cast< bool*>(_v)); break;
        case 55: setLineWidth(*reinterpret_cast< uint*>(_v)); break;
        case 56: setStartAngle(*reinterpret_cast< double*>(_v)); break;
        case 57: setRotation(*reinterpret_cast< double*>(_v)); break;
        case 58: setArcLength(*reinterpret_cast< double*>(_v)); break;
        case 59: setText(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 60;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 60;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 60;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 60;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 60;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 60;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 60;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
