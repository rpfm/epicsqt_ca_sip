/****************************************************************************
** Meta object code from reading C++ file 'QCaPeriodic.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../widgets/include/QCaPeriodic.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaPeriodic.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaPeriodic[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   13,   12,   12, 0x05,
      40,   13,   12,   12, 0x05,
      66,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      97,   82,   12,   12, 0x08,
     153,  135,   12,   12, 0x08,
     204,   12,   12,   12, 0x08,
     224,  218,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QCaPeriodic[] = {
    "QCaPeriodic\0\0out\0dbValueChanged(double)\0"
    "dbElementChanged(QString)\0requestResend()\0"
    "connectionInfo\0connectionChanged(QCaConnectionInfo&)\0"
    "value,alarmInfo,,\0"
    "setElement(double,QCaAlarmInfo&,QCaDateTime&,uint)\0"
    "userClicked()\0state\0requestEnabled(bool)\0"
};

void QCaPeriodic::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaPeriodic *_t = static_cast<QCaPeriodic *>(_o);
        switch (_id) {
        case 0: _t->dbValueChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 1: _t->dbElementChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->requestResend(); break;
        case 3: _t->connectionChanged((*reinterpret_cast< QCaConnectionInfo(*)>(_a[1]))); break;
        case 4: _t->setElement((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< QCaAlarmInfo(*)>(_a[2])),(*reinterpret_cast< QCaDateTime(*)>(_a[3])),(*reinterpret_cast< const uint(*)>(_a[4]))); break;
        case 5: _t->userClicked(); break;
        case 6: _t->requestEnabled((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaPeriodic::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaPeriodic::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_QCaPeriodic,
      qt_meta_data_QCaPeriodic, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaPeriodic::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaPeriodic::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaPeriodic::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaPeriodic))
        return static_cast<void*>(const_cast< QCaPeriodic*>(this));
    if (!strcmp(_clname, "QCaWidget"))
        return static_cast< QCaWidget*>(const_cast< QCaPeriodic*>(this));
    return QFrame::qt_metacast(_clname);
}

int QCaPeriodic::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void QCaPeriodic::dbValueChanged(const double & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QCaPeriodic::dbElementChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QCaPeriodic::requestResend()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
