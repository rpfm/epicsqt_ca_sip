/****************************************************************************
** Meta object code from reading C++ file 'QCaBitStatus.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../widgets/include/QCaBitStatus.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QCaBitStatus.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QCaBitStatus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       6,   39, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      45,   39,   13,   13, 0x0a,
      81,   66,   13,   13, 0x08,
     128,  119,   13,   13, 0x08,
     241,  184,   13,   13, 0x08,

 // properties: name, type, flags
     298,  290, 0x0a095003,
     307,  290, 0x0a095003,
     334,  329, 0x01095103,
     352,  329, 0x01095103,
     360,  329, 0x01095103,
     370,  329, 0x01095003,

       0        // eod
};

static const char qt_meta_stringdata_QCaBitStatus[] = {
    "QCaBitStatus\0\0out\0dbValueChanged(long)\0"
    "state\0requestEnabled(bool)\0connectionInfo\0"
    "connectionChanged(QCaConnectionInfo&)\0"
    "value,,,\0"
    "setBitStatusValue(long,QCaAlarmInfo&,QCaDateTime&,uint)\0"
    "variableNameIn,variableNameSubstitutionsIn,variableIndex\0"
    "useNewVariableNameProperty(QString,QString,uint)\0"
    "QString\0variable\0variableSubstitutions\0"
    "bool\0variableAsToolTip\0enabled\0allowDrop\0"
    "visible\0"
};

void QCaBitStatus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QCaBitStatus *_t = static_cast<QCaBitStatus *>(_o);
        switch (_id) {
        case 0: _t->dbValueChanged((*reinterpret_cast< const long(*)>(_a[1]))); break;
        case 1: _t->requestEnabled((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 2: _t->connectionChanged((*reinterpret_cast< QCaConnectionInfo(*)>(_a[1]))); break;
        case 3: _t->setBitStatusValue((*reinterpret_cast< const long(*)>(_a[1])),(*reinterpret_cast< QCaAlarmInfo(*)>(_a[2])),(*reinterpret_cast< QCaDateTime(*)>(_a[3])),(*reinterpret_cast< const uint(*)>(_a[4]))); break;
        case 4: _t->useNewVariableNameProperty((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QCaBitStatus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QCaBitStatus::staticMetaObject = {
    { &QBitStatus::staticMetaObject, qt_meta_stringdata_QCaBitStatus,
      qt_meta_data_QCaBitStatus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QCaBitStatus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QCaBitStatus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QCaBitStatus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QCaBitStatus))
        return static_cast<void*>(const_cast< QCaBitStatus*>(this));
    if (!strcmp(_clname, "QCaWidget"))
        return static_cast< QCaWidget*>(const_cast< QCaBitStatus*>(this));
    return QBitStatus::qt_metacast(_clname);
}

int QCaBitStatus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBitStatus::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getVariableNameProperty(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getVariableNameSubstitutionsProperty(); break;
        case 2: *reinterpret_cast< bool*>(_v) = getVariableAsToolTip(); break;
        case 3: *reinterpret_cast< bool*>(_v) = isEnabled(); break;
        case 4: *reinterpret_cast< bool*>(_v) = getAllowDrop(); break;
        case 5: *reinterpret_cast< bool*>(_v) = getRunVisible(); break;
        }
        _id -= 6;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setVariableNameProperty(*reinterpret_cast< QString*>(_v)); break;
        case 1: setVariableNameSubstitutionsProperty(*reinterpret_cast< QString*>(_v)); break;
        case 2: setVariableAsToolTip(*reinterpret_cast< bool*>(_v)); break;
        case 3: setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 4: setAllowDrop(*reinterpret_cast< bool*>(_v)); break;
        case 5: setRunVisible(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 6;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QCaBitStatus::dbValueChanged(const long & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
