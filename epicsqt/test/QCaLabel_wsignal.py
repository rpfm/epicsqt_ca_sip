from PyQt4 import QtGui, QtCore

from CaFramework import QCaLabel

class WidgetTest(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self)

        self.qca = QCaLabel("catest",self)
        
        self.qca.activate()

        hbox = QtGui.QHBoxLayout(self)
        hbox.addWidget(self.qca)
        self.setLayout(hbox)
        self.qca.dbValueChanged[str].connect(self.getValue)

    QtCore.pyqtSlot(str)
    def getValue(self, value):

        print ("updated {0}".format(value))


if __name__=="__main__":
    
    import sys
    app = QtGui.QApplication([])

    test = WidgetTest()
    test.show()
    sys.exit(app.exec_())
